# SW Dashboard - ReactJS and Redux

JavaScript application built with Dave Zuko's React Redux Starter Kit. Documentation can be found [here](https://github.com/davezuko/react-redux-starter-kit).

### Getting started

First, clone the project:

```bash
$ git clone https://bitbucket.org/jmcenteno/sw-dashboard-reactjs.git <my-project-name>
$ cd <my-project-name>
```

Then, install dependencies:

```bash
$ npm install
```

Finally, start the application:

```bash
$ npm start
```

Go to `http://localhost:8080` on your browser to view the project.

Username: demo@josecenteno.com
Password: #demo123!
