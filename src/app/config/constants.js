export const APP_NAME = 'SW Dashboard';
export const API_URL = 'http://swapi.co/api';
export const THEME_COLORS = ['#2C3E50', '#18bc9c', '#3498db', '#e74c3c', '#f39c12'];
export const LOCAL_STORAGE_PREFIX = 'sw-dashboard';
export const FIREBASE_CONFIG = {
	apiKey: "AIzaSyCiEbd6bXyzfivj3n8Ptd8lqWsgyYUYGW0",
	authDomain: "sw-dashboard-18275.firebaseapp.com",
	databaseURL: "https://sw-dashboard-18275.firebaseio.com",
};
