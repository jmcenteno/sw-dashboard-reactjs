import UserService from './user.service';
import User from './user.model';

export {
	UserService,
	User
}
