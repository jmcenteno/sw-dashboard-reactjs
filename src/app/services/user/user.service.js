import firebase from 'firebase';

import User from './user.model.js'

const db = firebase.database();

class UserService {

	getUserProfile () {

		return new Promise((resolve, reject) => {

			const user = firebase.auth().currentUser;

			if (user) {

				db.ref(`users/${user.uid}`).once('value')
					.then((snapshot) => {

						resolve(snapshot.val());

					});

			} else {

				resolve(new User());

			}

		});

	}

	updateUserProfile (profile) {

		const user = firebase.auth().currentUser;

		return db.ref(`users/${user.uid}`).set(profile);

	}

}

export default new UserService();
