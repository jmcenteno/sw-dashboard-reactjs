import BaseService from '../base.service';
import { Utils } from '../utils';

class VehiclesService extends BaseService {

	constructor () {

		super();

		this.url = `${this.baseUrl}/vehicles`;

	}

	getVehicles () {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/`)
				.then((response) => {

					Utils.getNextPage(response.body, response.body.results, resolve);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getVehicleDetails (id) {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/${id}/`)
				.then((response) => {

					const vehicle = Utils.addRecordId(response.body);
					
					resolve(vehicle);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

};

export default new VehiclesService();
