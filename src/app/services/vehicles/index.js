import VehiclesService from './vehicles.service';
import Vehicle from './vehicle.model';

export {
	VehiclesService,
	Vehicle
}
