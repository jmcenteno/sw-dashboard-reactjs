import StarshipsService from './starships.service';
import Starship from './starship.model';

export {
	StarshipsService,
	Starship
}
