import BaseService from '../base.service';
import { Utils } from '../utils';

class StarshipsService extends BaseService {

	constructor () {

		super();

		this.url = `${this.baseUrl}/starships`;

	}

	getStarships () {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/`)
				.then((response) => {

					Utils.getNextPage(response.body, response.body.results, resolve);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getStarshipDetails (id) {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/${id}/`)
				.then((response) => {

					const starships = Utils.addRecordId(response.body);
					
					resolve(starships);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

};

export default new StarshipsService();
