import _ from 'lodash';

import BaseService from '../base.service';
import { Utils } from '../utils';

class SpeciesService extends BaseService {

	constructor () {

		super();

		this.url = `${this.baseUrl}/species`;

	}

	getSpecies () {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/`)
				.then((response) => {

					Utils.getNextPage(response.body, response.body.results, resolve);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getSpeciesDetails (id) {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/${id}/`)
				.then((response) => {

					const species = Utils.addRecordId(response.body);
					
					resolve(species);

				})
				.catch((error) => {
					reject(error);
				});

		});		

	}

	getOptions (species) {

		const classifications = _.uniq(species.map((item) => {
			return item.classification;
		}));

		const designations = _.uniq(species.map((item) => {
			return item.designation;
		}));

		const languages = _.uniq(species.map((item) => {
			return item.language;
		}));

		const hairColors = _.uniq(_.flattenDeep(species.map((item) => {
			return item.hair_colors.split(', ');
		})));

		const skinColors = _.uniq(_.flattenDeep(species.map((item) => {
			return item.skin_colors.split(', ');
		})));

		const eyeColors = _.uniq(_.flattenDeep(species.map((item) => {
			return item.eye_colors.split(', ');
		})));

		return {
			classifications: classifications.sort(),
			designations: designations.sort(),
			languages: languages.sort(),
			hairColors: hairColors.sort(),
			skinColors: skinColors.sort(),
			eyeColors: eyeColors.sort()
		};

	}

};

export default new SpeciesService();
