import SpeciesService from './species.service';
import Species from './species.model';

export {
	SpeciesService,
	Species
}
