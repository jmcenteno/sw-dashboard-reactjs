import { Auth } from './auth';
import { AccountService } from './account';
import { MoviesService, Movie } from './movies';
import { PeopleService, Person } from './people';
import { SpeciesService, Species } from './species';
import { PlanetsService, Planet } from './planets';
import { VehiclesService, Vehicle } from './vehicles';
import { StarshipsService, Starship } from './starships';
import { StatisticsService } from './statistics';
import { UserService, User } from './user';
import { Utils } from './utils';

export {
	Auth,
	AccountService,
	MoviesService, 
	Movie,
	PeopleService, 
	Person,
	SpeciesService, 
	Species,
	PlanetsService, 
	Planet,
	VehiclesService,
	Vehicle,
	StarshipsService,
	Starship,
	StatisticsService,
	UserService,
	User,
	Utils
}
