import PeopleService from './people.service';
import Person from './person.model';

export {
	PeopleService,
	Person
}
