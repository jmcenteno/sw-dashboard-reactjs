import BaseService from '../base.service';
import { Utils } from '../utils';

class PeopleService extends BaseService {

	constructor () {

		super();

		this.url = `${this.baseUrl}/people`;

	}

	getPeople () {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/`)
				.then((response) => {

					Utils.getNextPage(response.body, response.body.results, resolve);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getPerson (id) {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/${id}/`)
				.then((response) => {
					
					const person = Utils.addRecordId(response.body);

					['hair', 'skin', 'eye'].forEach((key) => {
						person[key + '_color'] = person[key + '_color'].split(', ')[0];
					});

					this.request.get(person.species[0])
						.then((response) => {

							person.species = Utils.addRecordId(response.body);

							this.request.get(person.homeworld)
								.then((response) => {

									person.homeworld = Utils.addRecordId(response.body);

									resolve(person);

								});

						});

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

};

export default new PeopleService();
