class StatisticsService {

	getTotals (data) {

		const totals = {
			characters: 0,
			planets: 0,
			vehicles: 0,
			starships: 0
		};

		data.forEach(function (movie) {
			Object.keys(totals).forEach(function (key) {
				totals[key] += movie[key].length;
			});
		});

		return totals;

	}

	getGeneralStatistics (data) {

		const chartConfig = {
			chart: {
	            type: 'column',
	            spacingTop: 15,
	            spacingBottom: 30,
	            spacingLeft: 15,
	            spacingRight: 15,
	            height: 300
	        },
			title: {
				text: null
			},
			legend: {
				enabled: false
			},
			tooltip: {
	            headerFormat: `
	            	<strong>{point.key}</strong>
	            	<table class="table table-condensed" style="margin: 10px 0 0; min-width: 150px;">
	            `,
	            pointFormat: `
	            	<tr>
	            		<td style="color: {series.color}; padding: 0;">{series.name}:</td>
	            		<td class="text-right" style="padding: 0">
	            			<strong>{point.y}</strong>
	            		</td>
	            	</tr>
	            `,
	            footerFormat: `</table>`,
	            shared: true,
	            useHTML: true
	        },
	        colors: ['#18bc9c', '#3498db', '#e74c3c', '#f39c12'],
			xAxis: {
				categories: data.map(function (movie) {
					return movie.title;
				}),
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: null
				}
			},
			series: [
				{
		            name: 'Characters',
		            data: data.map(function (movie) {
		            	return movie.characters.length;
		            })
		        }, 
		        {
		            name: 'Planets',
		            data: data.map(function (movie) {
		            	return movie.planets.length;
		            })
		        }, 
				{
					name: 'Vehicles',
					data: data.map(function (movie) {
						return movie.vehicles.length;
					})
				}, 
		        {
		            name: 'Starships',
		            data: data.map(function (movie) {
		            	return movie.starships.length;
		            })
	        	}
	        ]
		};

		return chartConfig;

	}

	getStatsByKey (data, stat) {

		const chartConfig = {
			chart: {
				height: 300
			},
			title: {
				text: null,
			},
			xAxis: {
				categories: data.map(function (movie) {
					return movie.title;
				}),
				crosshair: true
			},
			yAxis: {
				title: {
					text: null
				},
				plotLines: [
					{
						value: 0,
						width: 1,
						color: '#808080'
					}
				]
			},
			tooltip: {}
		};

		const colors = ['#18bc9c', '#3498db', '#e74c3c', '#f39c12'];

		let series = [
			{
	            name: 'Characters',
	            color: colors[0],
	            data: data.map(function (movie) {
	            	return movie.characters.length;
	            })
	        }, 
	        {
	            name: 'Planets',
	            color: colors[1],
	            data: data.map(function (movie) {
	            	return movie.planets.length;
	            })
	        }, 
			{
				name: 'Vehicles',
				color: colors[2],
				data: data.map(function (movie) {
					return movie.vehicles.length;
				})
			}, 
	        {
	            name: 'Starships',
	            color: colors[3],
	            data: data.map(function (movie) {
	            	return movie.starships.length;
	            })
        	}
        ];

        if (stat != 'all') {

        	chartConfig.series = series.filter((item) => {
        		return (item.name == stat);
        	});

        } else {

        	chartConfig.series = series;

			chartConfig.legend = {
				layout: 'horizontal',
				align: 'left',
				borderWidth: 0
			};

		}

		return chartConfig;

	}

}

export default new StatisticsService();
