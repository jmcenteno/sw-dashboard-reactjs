import Validator from 'validator';

export const required = {
	// Function to validate value 
	// NOTE: value might be a number -> force to string 
	rule: (value) => {
		return value.toString().trim();
	},
	// Function to return hint 
	// You may use current value to inject it in some way to the hint 
	hint: (value) => {
		return (
			<span className="help-block text-danger">This field is required.</span>
		);
	}
};


export const email = {
	rule: value => {
		return Validator.isEmail(value);
	},
	hint: value => {
		return (
			<span className="help-block text-danger">{value} isnt an Email.</span>
		);
	}
};
