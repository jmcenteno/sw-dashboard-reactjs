import MoviesService from './movies.service';
import Movie from './movie.model';

export {
	MoviesService,
	Movie
}
