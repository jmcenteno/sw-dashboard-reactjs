import BaseService from '../base.service';
import { Utils } from '../utils';

class MoviesService extends BaseService {

	constructor () {

		super();

		this.url = `${this.baseUrl}/films`;

	}

	getMovies () {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/`)
				.then((response) => {
					
					Utils.getNextPage(response.body, response.body.results, resolve);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getMovieDetails (id) {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/${id}/`)
				.then((response) => {

					const movie = Utils.addRecordId(response.body);
					
					resolve(movie);

				})
				.catch((error) => {
					reject(error);
				});
				
		});

	}

}

export default new MoviesService();
