import store from 'store2';

import { LOCAL_STORAGE_PREFIX } from '../../config/constants';
import BaseService from '../base.service';

class AccountService extends BaseService {

	getUserProfile () {

		return new Promise((resolve, reject) => {

			const user = store.get(`${LOCAL_STORAGE_PREFIX}.profile`);

			if (user) {

				resolve({ profile: user });

			} else {

				reject(false);

			}

		});

	}

	updateUserProfile (profile) {

		return new Promise((resolve, reject) => {

			store.set(`${LOCAL_STORAGE_PREFIX}.profile`, profile);
			
			resolve(profile);

		});

	}

};

export default new AccountService();
