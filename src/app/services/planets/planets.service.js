import BaseService from '../base.service';
import { Utils } from '../utils';

class PlanetsService extends BaseService {

	constructor () {

		super();

		this.url = `${this.baseUrl}/planets`;

	}

	getPlanets () {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/`)
				.then((response) => {

					Utils.getNextPage(response.body, response.body.results, resolve);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getPlanetDetails (id) {

		return new Promise((resolve, reject) => {
			
			this.request.get(`${this.url}/${id}/`)
				.then((response) => {

					const planet = Utils.addRecordId(response.body);
					
					resolve(planet);

				})
				.catch((error) => {
					reject(error);
				});

		});

	}

	getOptions (planets) {

		const terrains = _.uniq(_.flattenDeep(planets.map((item) => {
			return item.terrain.split(', ');
		})));

		return {
			terrains: terrains.sort()
		};

	}

};

export default new PlanetsService();
