import PlanetsService from './planets.service';
import Planet from './planet.model';

export {
	PlanetsService,
	Planet
}
