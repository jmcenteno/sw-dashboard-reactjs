import firebase from 'firebase';
import store from 'store2';

import BaseService from '../base.service';
import { FIREBASE_CONFIG } from '../../config/constants';

class AuthService {

	authenticate (username, password) {

		return firebase.auth().signInWithEmailAndPassword(username, password);

	}

	isAuthenticated () {

		return (firebase.auth().currentUser);
		//return store.get(`firebase:authUser:${FIREBASE_CONFIG.apiKey}:[DEFAULT]`);

	}

	destroy () {

		return firebase.auth().signOut();

	}

};

export default new AuthService();
