import React from 'react';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

class SearchBar extends React.Component {

	static propTypes = {
		filter: React.PropTypes.string.isRequired,
		setFilter: React.PropTypes.func.isRequired
	};

	render() {

		return (
			<Panel>
				<FormGroup>
					<ControlLabel>Search</ControlLabel>
					<FormControl 
						value={this.props.filter} 
						onChange={(e) => this.props.setFilter(e.target.value)} 
					/>	
				</FormGroup>
				<button 
					type="button" 
					className="btn btn-link btn-xs" 
					onClick={() => this.props.setFilter('')}>
					Reset Filter
				</button>
			</Panel>
		);

	}

}

export default SearchBar;
