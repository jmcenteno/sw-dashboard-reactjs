import React from 'react';

class TableHeaderComponent extends React.Component {

	static propTypes = {
		columns: React.PropTypes.arrayOf(React.PropTypes.shape({
			key: React.PropTypes.string,
			label: React.PropTypes.string,
			sortable: React.PropTypes.bool
		})).isRequired,
		sortOrder: React.PropTypes.oneOf(['asc', 'desc', null]),
		sortBy: React.PropTypes.any,
		sortRows: React.PropTypes.func.isRequired
	};

	getSortIcon (col) {

		let sortIcon = null;

		if (col.sortable) {

			sortIcon = <i className="fa fa-sort sort-icon"></i>

			if (this.props.sortBy == col.key) {
				
				if (this.props.sortOrder == 'asc') {
					sortIcon = <i className="fa fa-sort-asc sort-icon"></i>
				} else if (this.props.sortOrder == 'desc') {
					sortIcon = <i className="fa fa-sort-desc sort-icon"></i>
				}

			} 

		}

		return sortIcon;

	}

	render() {
	
		return (
			<thead>
				<tr>
					{
						this.props.columns.map((col, i) => {

							let sortIcon = this.getSortIcon(col);

							return (
								col.sortable ?
								<th 
									key={i} 
									className="sortable" 
									onClick={() => this.props.sortRows(col.key)}>
									{col.label}
									{sortIcon}
								</th> : 
								<th key={i}>{col.label}</th>
							);

						})
					}
				</tr>
			</thead>
		);
	
	}

}

export default TableHeaderComponent;
