import React from 'react';

import TableHeader from './thead.component';
import TableBody from './tbody.component';

import './sortable-table.styles.scss';

class SortableTableComponent extends React.Component {

	static propTypes = {
		rows: React.PropTypes.array.isRequired,
		columns: React.PropTypes.arrayOf(React.PropTypes.shape({
			key: React.PropTypes.string,
			label: React.PropTypes.string,
			sortable: React.PropTypes.bool
		})).isRequired
	};

	constructor(props) {
	
		super(props);

		this.state = {
			rows: this.props.rows,
			sortBy: null,
			sortOrder: null,
			activePage: 1
		};
	
	}

	componentWillReceiveProps (newProps) {

		this.setState({ rows: newProps.rows });

	}

	getPaginatedItems(rows, page) {
	
		let paginatedItems = [];
		let counter = 0;
		let set = [];

		for (let i = 0; i < rows.length; i++) {
			
			set.push(rows[i]);
			
			if ((i + 1) % 10 == 0 || (i + 1) >= rows.length) {
				counter++;
				paginatedItems.push(set);
				set = [];
			}

		}
		
		return paginatedItems;

	}

	setActivePage (page) {

		this.setState({ activePage: page });

	}

	sortRows (key) {

		let sortOrder = 'asc';

		if (this.state.sortBy == key) {
			
			if (this.state.sortOrder == null) {
				sortOrder = 'asc';
			} else if (this.state.sortOrder == 'asc') {
				sortOrder = 'desc';
			} else if (this.state.sortOrder == 'desc') {
				sortOrder = null;
				key = null;
			}

		}

		let rows = this.state.rows;

		if (sortOrder) {

			rows = _.sortBy(rows, [key]);

			if (sortOrder == 'desc') {
				rows = rows.reverse();
			}

		} else {
			rows = this.props.rows;
		}

		this.setState({
			rows: rows,
			sortBy: key,
			sortOrder: sortOrder
		});

	}

	render() {

		const {
			rows,
			columns,
			...props
		} = this.props;

		const pages = this.getPaginatedItems(this.state.rows, this.state.activePage);
		console.log(pages);

		return (
			<div className="sortable-table">
				
				<table className="table">
					<TableHeader 
						columns={columns} 
						sortBy={this.state.sortBy} 
						sortOrder={this.state.sortOrder}
						sortRows={this.sortRows.bind(this)} 
					/>
					<TableBody
						columns={columns}
						//rows={this.state.rows}
						rows={ pages[this.state.activePage-1] || pages[pages.length-1] || rows }
					/>
				</table>
				
				{
					this.state.rows.length > 10 ?
					<nav className="sortable-table-pagination" aria-label="Page navigation">
						<a 
							className="btn btn-default btn-sm" 
							disabled={this.state.activePage == 1} 
							onClick={() => this.setActivePage(1)}
							title="First">
							<i className="fa fa-angle-double-left"></i>
						</a>
						<a 
							className="btn btn-default btn-sm" 
							disabled={this.state.activePage == 1}
							onClick={() => this.setActivePage(this.state.activePage <= 1 ? 1 : this.state.activePage - 1)}
							title="Previous">
							<i className="fa fa-angle-left"></i>
						</a>
						<a 
							className="btn btn-default btn-sm" 
							disabled={this.state.activePage == pages.length}
							onClick={() => this.setActivePage(this.state.activePage >= pages.length ? pages.length : this.state.activePage + 1)}
							title="Next">
							<i className="fa fa-angle-right"></i>
						</a>
						<a 
							className="btn btn-default btn-sm" 
							disabled={this.state.activePage == pages.length}
							onClick={() => this.setActivePage(pages.length)}
							title="Last">
							<i className="fa fa-angle-double-right"></i>
						</a>
					</nav> :
					null
				}

			</div>
		);
	}
}

export default SortableTableComponent;
