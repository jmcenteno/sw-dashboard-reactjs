import React from 'react';

import './page.styles.scss';

export default class Page extends React.Component {

	static propTypes = {
		header: React.PropTypes.element,
		className: React.PropTypes.string
	};

	static defaultProps = {
		className: ''
	};

	render () {

		return (
			<div className={'page ' + this.props.className}>
				{
					this.props.header ? 
					<div className="page-header">
						{this.props.header}
					</div> :
					null
				}
				<div className="page-content">
					{this.props.children}
				</div>
			</div>
		);

	}

}
