import React from 'react';

import { APP_NAME } from '../../config/constants'; 

class Footer extends React.Component {

	render() {
		
		return (
			<footer className="footer">
				<div className="container-fluid">
					<div className="text-center">
						<p>
							&copy;
							{' ' + (new Date()).getFullYear() + ' ' + APP_NAME + ' - All rights reserved.'}
							
						</p>
						<p>
							<em>Powered by <a href="http://swapi.co" target="_blank">SWAPI</a></em>
						</p>
						<br/>
					</div>
				</div>
			</footer>
		);

	}

}

export default Footer;
