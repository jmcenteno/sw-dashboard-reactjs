import React from 'react';
import { IndexLink, Link } from 'react-router';

import './main-menu.styles.scss';

export default class MainMenu extends React.Component {

	static propTypes = {
		open: React.PropTypes.bool
	};

	render () {

		return (
			<div className="main-menu">
				<div className="scrollbar">
					<ul className="main-menu-accordion">
						<li className="nav-title">Main Menu</li>
						<li>
							<Link to="/" activeClassName="active" onlyActiveOnIndex={true}>
								<i className="fa fa-home"></i>
								Dashboard
							</Link>
						</li>
						<li>
							<Link to="/movies" activeClassName="active">
								<i className="fa fa-film"></i>
								Movies
							</Link>
						</li>
						<li>
							<Link to="/people" activeClassName="active">
								<i className="fa fa-users"></i>
								People
							</Link>
						</li>
						<li>
							<Link to="/species" activeClassName="active">
								<i className="fa fa-list"></i>
								Species
							</Link>
						</li>
						<li>
							<Link to="/planets" activeClassName="active">
								<i className="fa fa-globe"></i>
								Planets
							</Link>
						</li>
						{/*<li>
							<Link to="/vehicles" activeClassName="active">
								<i className="fa fa-car"></i>
								Vehicles
							</Link>
						</li>*/}
						{/*<li>
							<Link to="/starships" activeClassName="active">
								<i className="fa fa-rocket"></i>
								Starships
							</Link>
						</li>*/}
					</ul>
				</div>
			</div>
		);

	}

};
