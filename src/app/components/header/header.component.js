import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Nav, NavDropdown, MenuItem } from 'react-bootstrap';

import { APP_NAME } from '../../config/constants';
import { Auth, User, UserService } from '../../services';

import './header.styles.scss';

export class Header extends React.Component {

	static propTypes = {
		user: React.PropTypes.any
	};

	constructor (props) {

		super(props);

		this.state = {
			active: false
		};

	}

	toggleMenu () {

		let menu = document.querySelector('.main-menu');

		if (menu) {

			let className = menu.getAttribute('class');

			if (className.indexOf(' open') == -1) {
				menu.setAttribute('class', className + ' open');
			} else {
				menu.setAttribute('class', className.replace(' open', ''));
			}

			this.setState({ active: !this.state.active });

		}

	}

	getUser () {

		const { user } = this.props;

		return (
			<span>
				<img 
					src={user.picture ? 'data:image/png;base64,' + user.picture : '../img/no-img.png'} 
					alt={`${user.firstName} ${user.lastName}`} 
					className={'avatar ' + (!user.picture ? 'img-circle' : '')} 
				/>
				{
					user ?
					<span className="hidden-xs">{`${user.firstName} ${user.lastName}`}</span> :
					null
				}
			</span>
		);

	}

	logOut () {

		Auth.destroy().then(() => {
			browserHistory.push('/login');
		});

	}

	render () {

		const user = this.getUser();

		return (
			<div className="navbar navbar-custom navbar-fixed-top">
				<div className="container-fluid">
					<div className="navbar-header">
						<div className="brand-container">
							<button 
								type="button" 
								className={'navbar-toggle ' + (this.state.active ? 'active' : '')} 
								onClick={this.toggleMenu.bind(this)}>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
							<Link to="/" className="navbar-brand">
								<i className="fa fa-rebel icon"></i> 
								{APP_NAME}
							</Link>
							<div className="clearfix"></div>
						</div>
					</div>
					<Nav className="navbar-nav navbar-right">
						<NavDropdown eventKey={3} title={user} id="user-menu">
							<MenuItem onClick={() => browserHistory.push('/account')}>
								<i className="fa fa-user icon"></i>
								My Account
							</MenuItem>
							<MenuItem divider></MenuItem>
							<MenuItem onClick={() => this.logOut()}>
								<i className="fa fa-sign-out icon"></i>
								Log Out
							</MenuItem>
						</NavDropdown>
					</Nav>
				</div>
			</div>
		);

	}

}

export default Header;
