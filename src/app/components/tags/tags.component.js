import React from 'react';

class TagsComponent extends React.Component {
	
	static propTypes = {
		name: React.PropTypes.string,
	};

	static defaultProps = {
		tags: []
	};

	constructor(props) {
		
		super(props);

		this.state = {
			tags: this.props.tags 
		};

	}

	addTag (value) {

		if (e.keyCode === 13 && e.target.value != '') {
			
			const tags = this.state.tags;
			
			if (tags.indexOf(e.target.value) == -1) {

				tags.push(e.target.value);
				
				this.setState({ tags: tags });
				
				e.target.value = '';

			}

			this.props.onAddTag(tags);

		}

		return false;

	}

	removeTag () {

	}

	removeAllTags () {
		
	}

	render() {

		const {
			tags,
			...props
		} = this.props;

		return (
			<div className="tag-input">
				
				<input type="text" className="form-control" onKeyDown={(e) => this.addTag(e.target.value)} /> 
				
				<div className="help-block">
					<span>Press <code>`Enter`</code> to add a new tag.</span>
				</div>

				<div className="tags">
					{
						this.state.tags.map((item, i) => {
							return (
								<div className="label" key={i}>
									{item}
									<a className="remove-tag" onClick={(e) => this.removeTag(item)}>
										<i className="fa fa-times"></i>
									</a>
								</div>
							);
						})
					}
				</div>
				
				{
					this.state.tags.length ?
					<button 
						type="button" 
						className="btn btn-link btn-sm" 
						onClick={() => this.removeAllTags()}>
						Remove All
					</button> : 
					null
				}

			</div>
		);
	}
}

export default TagsComponent;
