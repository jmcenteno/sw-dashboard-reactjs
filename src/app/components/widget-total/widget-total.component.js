import React from 'react';

import './widget-total.styles.scss';

export default class WidgetTotal extends React.Component {

    static propTypes = {
        title: React.PropTypes.string.isRequired,
        bsStyle: React.PropTypes.oneOf(['primary', 'success', 'warning', 'danger', 'info']),
		className: React.PropTypes.string,
		iconClass: React.PropTypes.string
    };

    static defaultProps = {
    	bsStyle: 'primary',
    	className: '',
    	iconClass: ''
    };

    constructor(props) {

        super(props);

    }

    render() {

    	const {
    		title,
    		bsStyle,
    		className,
    		iconClass,
    		children
    	} = this.props;
 
        return (
        	<div className={`widget-total panel panel-${bsStyle} ${className}`}>
        		<div className="panel-body">
	        		<div className="media">
	  					<div className="media-left media-middle">
	  						<div className="icon media-object">
	  							<i className={iconClass}></i>
	  						</div>
	  					</div>
	  					<div className="media-body">
                            <div className="total">{children}</div>
	  						<h4 className="media-heading">{title}</h4>
	  					</div>
	  				</div>
	  			</div>
        	</div>
        );
    }

}
