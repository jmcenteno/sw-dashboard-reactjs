import React from 'react';

import './widget-panel.styles.scss';

export default class WigetPanel extends React.Component {

	constructor (props) {
	
		super(props);
	
		this.state = {
			open: true
		};
	
	}

	static propTypes = {
		bsStyle: React.PropTypes.oneOf(['default', 'primary', 'success', 'warning', 'danger', 'info']),
		className: React.PropTypes.string,
		title: React.PropTypes.string.isRequired
	};

	static defaultProps = {
		bsStyle: 'default',
		className: ''
	};

	toggleCollapse (e) {

		this.setState({ open: !this.state.open });

	}

	render () {

		const { 
			bsStyle,
			className,
			title,
			children 
		} = this.props;

		return (
			<div className={`widget-panel panel panel-${bsStyle} ${className}`}>
	 			<div className="panel-heading clearfix">
	 				<h3 className="panel-title pull-left">{title}</h3>	
 					<ul className="panel-tools pull-right list-inline">
						<li>
							<a 
								href="javascript:;" 
								className="panel-collapse" 
								onClick={this.toggleCollapse.bind(this)} 
								title="Collapse Panel">
								<i className={'fa ' + (this.state.open ? 'fa-angle-up' : 'fa-angle-down')}></i>
							</a>
						</li>
					</ul>
	 			</div>
	 			<div className={'panel-body collapse ' + (this.state.open ? 'in' : '')}>
					{children}
				</div>
			</div>
		);

	}

}
