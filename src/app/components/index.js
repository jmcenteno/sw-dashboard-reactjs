import Header from './header';
import Footer from './footer';
import MainMenu from './main-menu';
import Page from './page';
import WidgetPanel from './widget-panel';
import WidgetTotal from './widget-total';
import Spinner from './spinner';
import Tags from './tags';
import SortableTable from './sortable-table';
import SearchBar from './search-bar';

export { 
	Header,
	Footer,
	MainMenu,
	Page,
	WidgetPanel,
	WidgetTotal,
	Spinner,
	Tags,
	SortableTable,
	SearchBar
}
