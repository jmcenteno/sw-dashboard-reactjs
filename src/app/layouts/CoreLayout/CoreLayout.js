import React from 'react';
import { browserHistory } from 'react-router';

import { APP_NAME } from '../../config/constants';
import { Auth, UserService, User } from '../../services';
import { Header, MainMenu, Footer } from '../../components';

import './CoreLayout.scss';
import '../../../scss/styles.scss';

export class CoreLayout extends React.Component {

	static propTypes = {
		children: React.PropTypes.element.isRequired
	};

	componentDidMount() {

		if (!Auth.isAuthenticated()) {
			browserHistory.push('/login');
		}

	}

	componentWillReceiveProps(newProps) {

		UserService.getUserProfile()
			.then((user) => {
				this.currentUser = user;
				this.forceUpdate();
			});

	}

	render() {

		return (
			Auth.isAuthenticated() ?
				<div style={{ minHeight: '100%' }}>
					<Header user={this.currentUser || new User()} />
					<MainMenu />
					<div className="main-content">
						<main>
							{this.props.children}
						</main>
						<Footer />
					</div>
				</div> :
				<div style={{ minHeight: '100%' }}>
					{this.props.children}
					<Footer />
				</div>
		);

	}

}

export default CoreLayout;
