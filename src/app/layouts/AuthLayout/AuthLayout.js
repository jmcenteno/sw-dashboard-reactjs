import React from 'react'
import './AuthLayout.scss'
import '../../../scss/styles.scss'

export const AuthLayout = ({ children }) => (
	<div className='container'>
		<div className='auth-layout__viewport'>
			{children}
		</div>
	</div>
)

AuthLayout.propTypes = {
	children: React.PropTypes.element.isRequired
}

export default AuthLayout
