import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import Color from 'color';

import { THEME_COLORS } from '../../../../config/constants';

class DoughnutChart extends React.Component {

	static propTypes = {
		people: React.PropTypes.number.isRequired,
		species: React.PropTypes.number.isRequired,
		planets: React.PropTypes.number.isRequired,
		vehicles: React.PropTypes.number.isRequired,
		starships: React.PropTypes.number.isRequired,
	};

	render () {

		const data = {
			labels: ['Characters', 'Species', 'Planets', 'Vehicles', 'Starships'],
			datasets: [
				{
					backgroundColor: THEME_COLORS,
					hoverBackgroundColor: THEME_COLORS,
					hoverBorderColor: THEME_COLORS,
					data: [
						this.props.people,
						this.props.species,
						this.props.planets,
						this.props.vehicles,
						this.props.starships
					]
				}
			]
		};

		const options = {
			legend: {
				position: (window.innerWidth >= 992 ? 'bottom' : 'right')
			}
		};
	
		return (
			<Doughnut data={data} options={options} />
		);
	
	}

}

export default DoughnutChart;
