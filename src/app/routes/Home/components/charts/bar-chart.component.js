import React from 'react';
import { Bar } from 'react-chartjs-2';
import Color from 'color';
import _ from 'lodash';

import { THEME_COLORS } from '../../../../config/constants';

class BarChart extends React.Component {
	
	static propTypes = {
		movies: React.PropTypes.array.isRequired,
		selectedMovie: React.PropTypes.object,
		selectMovie: React.PropTypes.func.isRequired
	};

	selectMovie (id) {

		const movie = this.props.movies.find((item) => {
			return (item.id == id);
		});

		this.props.selectMovie(movie);

	}

	getMaxValue () {

		let values = _.uniq(_.flatten(this.props.movies.map((movie) => {
			return [
				movie.characters.length, 
				movie.species.length, 
				movie.planets.length, 
				movie.vehicles.length, 
				movie.starships.length
			];
		})));

		values = values.sort(function (a,b) {
			return a - b;
		});

		return values[values.length-1];

	}

	render() {

		const data = {
			labels: ['People', 'Species', 'Planets', 'Vehicles', 'Starships'],
			datasets: [
				{
					label: this.props.selectedMovie.title,
					borderColor: THEME_COLORS.map((color) => Color(color).alpha(0.8)),
					hoverBorderColor: THEME_COLORS,
					borderWidth: 2,
					backgroundColor: THEME_COLORS.map((color) => Color(color).alpha(0.4)),
	 				hoverBackgroundColor: THEME_COLORS,
					data: [
						this.props.selectedMovie.characters.length,
						this.props.selectedMovie.species.length,
						this.props.selectedMovie.planets.length,
						this.props.selectedMovie.vehicles.length,
						this.props.selectedMovie.starships.length
					]
				}
			]	
		};

		const options = {
			legend: {
				display: false
			},
			scales: {
				yAxes: [
					{
						ticks: {
							beginAtZero: true,
							max: this.getMaxValue()
						}
					}
				]
			}
		};
		
		return (
			<div>
				<div className="row">
					<div className="col-md-6">
						<div className="form-group">
							<select 
								className="form-control" 
								onChange={(e) => this.selectMovie(e.target.value)}>
								{
									this.props.movies.map((movie, i) => {
										return (
											<option key={i} value={movie.id}>
												{movie.title}
											</option>
										);
									})
								}
							</select>
						</div>
					</div>
				</div>
				<Bar data={data} options={options} />
			</div>
		);
	
	}

}

export default BarChart;
