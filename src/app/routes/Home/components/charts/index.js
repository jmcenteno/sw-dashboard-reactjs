import BarChart from './bar-chart.component';
import DoughnutChart from './doughnut-chart.component';
import LineChart from './line-chart.component';

export {
	BarChart,
	DoughnutChart,
	LineChart
}
