import React from 'react';
import { Line } from 'react-chartjs-2';
import Color from 'color';

import { THEME_COLORS } from '../../../../config/constants';

class LineChart extends React.Component {

	static propTypes = {
		movies: React.PropTypes.array.isRequired,
	};

	render () {

		const datasets = [
			{ key: 'characters', 'label': 'People' },
			{ key: 'species', 'label': 'Species' },
			{ key: 'planets', 'label': 'Planets' },
			{ key: 'vehicles', 'label': 'Vehicles' },
			{ key: 'starships', 'label': 'Starships' }
		];

		const data = {
			labels: this.props.movies.map((movie) => movie.title),
			datasets: datasets.map((item, i) => {
				return {
					label: item.label,
					borderColor: THEME_COLORS[i],
					hoverBorderColor: THEME_COLORS[i],
					backgroundColor: Color(THEME_COLORS[i]).alpha(0.1),
					pointHoverBackgroundColor: Color(THEME_COLORS[i]).alpha(0.5),
					data: this.props.movies.map((movie) => movie[item.key].length)
				}
			})
		};

		const options = {
			legend: {
				position: (window.innerWidth >= 992 ? 'bottom' : 'right')
			}
		};

		return (
			<Line data={data} options={options} />
		);

	}

}

export default LineChart;
