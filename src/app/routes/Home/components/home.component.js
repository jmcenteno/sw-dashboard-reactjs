import React from 'react';
import { Link } from 'react-router';
import { Panel, Accordion } from 'react-bootstrap';
import { Line, Doughnut } from 'react-chartjs-2';
import Color from 'color';

import { Page, WidgetTotal, Spinner, SortableTable } from '../../../components';
import { StatisticsService } from '../../../services';

import * as Charts from './charts';

import './home.styles.scss'

export class HomeView extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			selectedStats: 'all'
		};

	} 

	componentDidMount () {

		this.props.actions.getDashboardData();

	}

	getTotals () {

		const totals = {
			characters: 0,
			species: 0,
			planets: 0,
			vehicles: 0,
			starships: 0
		};

		this.props.movies.forEach((movie) => {
			Object.keys(totals).forEach((key) => {
				totals[key] += movie[key].length;
			});
		});

		return totals;

	}

	getTableData () {

		const columns = [
			{ key: 'name', label: 'Name', sortable: true },
			{ key: 'films', label: 'Films', sortable: true }
		];

		const obj = {};

		['people', 'species', 'planets', 'vehicles', 'starships'].forEach((key) => {
			obj[key] = {
				columns: columns,
				rows: this.props[key]
			}
		});

		return obj;

	}

	render () {

		const totals = this.getTotals();
		const tables = this.getTableData();
	
		return (
			<Page title="Dashboard">

				{
					this.props.movies.length ?
					<div className="row">
						<div className="col-lg-6">
							<div className="row">
								<div className="col-sm-6">
									<WidgetTotal title="Characters" bsStyle="success" iconClass="fa fa-users">
										{totals.characters}
									</WidgetTotal>
								</div>
								<div className="col-sm-6">
									<WidgetTotal title="Planets" bsStyle="info" iconClass="fa fa-globe">
										{totals.planets}
									</WidgetTotal>
								</div>
							</div>
						</div>
					 	<div className="col-lg-6">
							<div className="row">
								<div className="col-sm-6">
									<WidgetTotal title="Vehicles" bsStyle="danger" iconClass="fa fa-car">
										{totals.vehicles}
									</WidgetTotal>
								</div>
								<div className="col-sm-6">
									<WidgetTotal title="Starships" bsStyle="warning" iconClass="fa fa-rocket">
										{totals.starships}
									</WidgetTotal>
								</div>
							</div>
						</div>
					</div> :
					<div style={{ padding: '20px' }}>
						<Spinner />
					</div>
				}

				<div className="row">
					<div className="col-lg-8">

						<Panel 
							header={<h3 className="panel-title">Stats by Movie</h3>}
							className={this.props.movies.length ? 'chart-panel' : ''}>
							{
								this.props.movies.length ?
								<Charts.BarChart 
									movies={this.props.movies} 
									selectedMovie={this.props.selectedMovie || this.props.movies[0]}
									selectMovie={this.props.actions.selectMovie.bind(this)}
								/> :
								<Spinner />
							}
						</Panel>

						<Panel 
		 					className={this.props.movies.length ? 'chart-panel' : ''}
							header={<h3 className="panel-title">More Statistics</h3>}>
							{
								this.props.movies.length ? 
								<Charts.LineChart movies={this.props.movies} /> :
								<Spinner />
							}
						</Panel>

					</div>
					<div className="col-lg-4">

						<Panel 
							header={<h3 className="panel-title">Combined Totals</h3>}
							className={this.props.movies.length ? 'chart-panel' : ''}>
							{
								this.props.movies.length ?
								<Charts.DoughnutChart 
									people={totals.characters}
									species={totals.species}
									planets={totals.planets}
									vehicles={totals.vehicles}
									starships={totals.starships} 
								/> :
								<Spinner />
							}
						</Panel>

						<Accordion>
							<Panel header={<h3 className="panel-title">Popular Characters</h3>} eventKey={1} expanded={true}>
								<SortableTable columns={tables.people.columns} rows={tables.people.rows} />
								<Link to="/people" className="btn btn-default btn-sm">View All Characters</Link>
							</Panel>
							<Panel header={<h3 className="panel-title">Popular Species</h3>} eventKey={2}>
								<SortableTable columns={tables.species.columns} rows={tables.species.rows} />
								<Link to="/people" className="btn btn-default btn-sm">View All Species</Link>
							</Panel>
							<Panel header={<h3 className="panel-title">Popular Planets</h3>} eventKey={3}>
								<SortableTable columns={tables.planets.columns} rows={tables.planets.rows} />
								<Link to="/people" className="btn btn-default btn-sm">View All Planets</Link>
							</Panel>
							<Panel header={<h3 className="panel-title">Popular Vehicles</h3>} eventKey={4}>
								<SortableTable columns={tables.vehicles.columns} rows={tables.vehicles.rows} />
								<Link to="/people" className="btn btn-default btn-sm">View All Vehicles</Link>
							</Panel>
							<Panel header={<h3 className="panel-title">Popular Starships</h3>} eventKey={5}>
								<SortableTable columns={tables.starships.columns} rows={tables.starships.rows} />
								<Link to="/people" className="btn btn-default btn-sm">View All Starships</Link>
							</Panel>
						</Accordion>

					</div>
				</div>

			</Page>
		);

	}

}

export default HomeView;
