import _ from 'lodash';

import {
	MoviesService,
	PeopleService,
	SpeciesService,
	PlanetsService,
	VehiclesService,
	StarshipsService
} from '../../../services';
import * as Constants from './constants';

export function getDashboardData () {
	return (dispatch) => {

		dispatch(getMovies());
		dispatch(getPeople());
		dispatch(getSpecies());
		dispatch(getPlanets());
		dispatch(getVehicles());
		dispatch(getStarships());

	};
}

export function getMovies () {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.GET_MOVIES,
			payload: []
		});

		MoviesService.getMovies()
			.then((data) => {

				dispatch({
					type: Constants.GET_MOVIES,
					payload: _.sortBy(data, ['episode_id'])
				});

			});

	};
}

export function selectMovie (movie) {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.SET_SELECTED_MOVIE,
			payload: movie
		});

	};
}

export function getPeople () {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.GET_DASBOARD_PEOPLE,
			payload: []
		});

		PeopleService.getPeople()
			.then((data) => {

				data = _.take(data.map((item) => {
					return { name: item.name, films: item.films.length }
				}), 5);

				dispatch({
					type: Constants.GET_DASBOARD_PEOPLE,
					payload: _.sortBy(data, ['films']).reverse()
				});

			});

	};
}

export function getSpecies () {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.GET_DASBOARD_SPECIES,
			payload: []
		});

		SpeciesService.getSpecies()
			.then((data) => {

				data = _.take(data.map((item) => {
					return { name: item.name, films: item.films.length }
				}), 5);

				dispatch({
					type: Constants.GET_DASBOARD_SPECIES,
					payload: _.sortBy(data, ['films']).reverse()
				});

			});

	};
}

export function getPlanets () {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.GET_DASBOARD_PLANETS,
			payload: []
		});

		PlanetsService.getPlanets()
			.then((data) => {

				data = _.take(data.map((item) => {
					return { name: item.name, films: item.films.length }
				}), 5);

				dispatch({
					type: Constants.GET_DASBOARD_PLANETS,
					payload: _.sortBy(data, ['films']).reverse()
				});

			});

	};
}

export function getVehicles () {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.GET_DASBOARD_VEHICLES,
			payload: []
		});

		VehiclesService.getVehicles()
			.then((data) => {

				data = _.take(data.map((item) => {
					return { name: item.name, films: item.films.length }
				}), 5);

				dispatch({
					type: Constants.GET_DASBOARD_VEHICLES,
					payload: _.sortBy(data, ['films']).reverse()
				});

			});

	};
}

export function getStarships () {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.GET_DASBOARD_STARSHIPS,
			payload: []
		});

		StarshipsService.getStarships()
			.then((data) => {

				data = _.take(data.map((item) => {
					return { name: item.name, films: item.films.length }
				}), 5);

				dispatch({
					type: Constants.GET_DASBOARD_STARSHIPS,
					payload: _.sortBy(data, ['films']).reverse()
				});

			});

	};
}

export const actions = {
	getMovies,
	selectMovie,
	getPeople,
	getSpecies,
	getPlanets,
	getVehicles,
	getStarships
}
