import * as Constants from './constants';

const ACTION_HANDLERS = {
	
	[Constants.GET_MOVIES]: (state, action) => {
		return Object.assign({}, state, {
			movies: action.payload
		});
	},

	[Constants.SET_SELECTED_MOVIE]: (state, action) => {
		return Object.assign({}, state, {
			selectedMovie: action.payload
		});
	},

	[Constants.GET_DASBOARD_PEOPLE]: (state, action) => {
		return Object.assign({}, state, {
			people: action.payload
		});
	},

	[Constants.GET_DASBOARD_SPECIES]: (state, action) => {
		return Object.assign({}, state, {
			species: action.payload
		});
	},

	[Constants.GET_PDASBOARD_LANETS]: (state, action) => {
		return Object.assign({}, state, {
			planets: action.payload
		});
	},

	[Constants.GET_DASBOARD_VEHICLES]: (state, action) => {
		return Object.assign({}, state, {
			vehicles: action.payload
		});
	},

	[Constants.GET_DASBOARD_STARSHIPS]: (state, action) => {
		return Object.assign({}, state, {
			starships: action.payload
		});
	},

}

const initialState = {
	movies: [],
	selectedMovie: null,
	people: [],
	species: [],
	planets: [],
	vehicles: [],
	starships: []
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
