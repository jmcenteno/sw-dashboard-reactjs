import { UserService } from '../../../services';
import * as Constants from './constants';

export function getAccount () {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.GET_ACCOUNT,
            payload: {}
        });
        
        UserService.getUserProfile()
            .then((data) => {
                
                dispatch({
                    type: Constants.GET_ACCOUNT,
                    payload: {
                        profile: data
                    }
                });
           
            });
        
    };
}

export function updateUserProfile (profile) {
    return (dispatch, getState) => {
        
        UserService.updateUserProfile(profile)
            .then((data) => {
                
                dispatch({
                    type: Constants.UPDATE_PROFILE,
                    payload: profile
                });
           
            })
            .catch(() => {

            	dispatch({
                    type: Constants.PROFILE_UPDATED,
                    payload: false
                });

            });

    };
}

export function dismissAlert () {
	return (dispatch, getState) => {

		dispatch({
            type: Constants.PROFILE_UPDATED,
            payload: null
        });      

	};
}

export const actions = {
    getAccount,
    updateUserProfile,
    dismissAlert
}
