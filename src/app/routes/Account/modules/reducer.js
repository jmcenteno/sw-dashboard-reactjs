import * as Constants from './constants';

const ACTION_HANDLERS = {
	
	[Constants.GET_ACCOUNT]: function (state, action) {
		return Object.assign({}, state, {
			account: action.payload || {}
		});
	},

	[Constants.UPDATE_PROFILE]: function (state, action) {
		return Object.assign({}, state, {
			account: {
				profile: action.payload
			},
			updated: true
		});
	},

	[Constants.PROFILE_UPDATED]: function (state, action) {
		return Object.assign({}, state, {
			updated: action.payload
		});
	},

}

const initialState = {
	account: {},
	updated: null
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
