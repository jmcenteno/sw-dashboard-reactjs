import React from 'react';
import { Tabs, Tab } from 'react-bootstrap';

import { Page, Spinner } from '../../../components';
import { Profile } from './profile';
import { Credentials } from './credentials';

import './account.styles.scss';

export class AccountView extends React.Component {

	componentDidMount () {
		
		this.props.actions.getAccount();

	}

	getPageHeader () {

		return (
			<div className="row">
				<div className="col-sm-8">
					<h1 className="page-title pull-left">My Account</h1>
				</div>
				<div className="col-sm-4">
				</div>
			</div>
		);

	}

	render () {

		const pageHeader = this.getPageHeader();

		return (
			<Page header={pageHeader}>

				<Tabs id="user-tabs" bsStyle="pills" defaultActiveKey={1} animation={false}>
					<Tab title="Profile" eventKey={1}>
						{
							this.props.account && this.props.account.profile ?
							<Profile 
								data={this.props.account.profile}
								updated={this.props.updated}
								onSubmit={this.props.actions.updateUserProfile.bind(this)}
								onReset={this.props.actions.getAccount.bind(this)}
								dismissAlert={this.props.actions.dismissAlert.bind(this)}
							/> :
							null
						}
					</Tab>
					<Tab title="Credentials" eventKey={2}>
						<Credentials />
					</Tab>
				</Tabs>

			</Page>
		);

	}

}

export default AccountView;
