import React from 'react';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';
import { NotificationContainer, NotificationManager } from 'react-notifications';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	},
	password: {
		// rule function can accept argument:
		// components - components registered to Form mapped by name
		rule: (value, components) => {
			const password = components.password.state;
			const passwordConfirm = components.passwordConfirm.state;
			const isBothUsed = password
				&& passwordConfirm
				&& password.isUsed
				&& passwordConfirm.isUsed;
			const isBothChanged = isBothUsed && password.isChanged && passwordConfirm.isChanged;

			if (!isBothUsed || !isBothChanged) {
				return true;
			}

			return password.value === passwordConfirm.value;
		},
		hint: () => {
			return (
				<span className="help-block text-danger">Passwords should be equal.</span>
			);
		}
	},
});

class Credentials extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			username: 'demo',
			password: '#demo123!'
		};

	}

	componentDidUpdate () {

		if (this.state.updated) {
			
			NotificationManager.success('Your profile has been updated.', 'Success!');

			setTimeout(() => {
				this.setState({ updated: false });
			}, 3000);

		}

	}

	handleSubmit (e) {

		e.preventDefault();

		this.setState({ updated: true })

	}

	handleReset () {

		['password', 'passwordConfirm'].forEach((field) => {
			this.form.components[field].setState({ value: "" });
		});

	}

	render () {

		return (
			<Panel>
				
				<Validation.components.Form 
					ref={(c) => { this.form = c }} 
					onSubmit={(e) => this.handleSubmit(e)}>

					<div className="row">
						<div className="col-sm-6">

							<FormGroup controlId="username">
								<ControlLabel>Username</ControlLabel>
								<Validation.components.Input 
									value={this.state.username} 
									name="username" 
									ref="username"
									className="form-control" 
									disabled={true}
									errorClassName="invalid"
									validations={['required']} 
								/>
							</FormGroup>

							<FormGroup controlId="password">
								<ControlLabel>Password</ControlLabel>
								<Validation.components.Input 
									type="password"
									value={""} 
									name="password" 
									ref="password" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']} 
								/>
							</FormGroup>

							<FormGroup controlId="passwordConfirm">
								<ControlLabel>Confirm Password</ControlLabel>
								<Validation.components.Input 
									type="passwordConfirm"
									value={""} 
									name="passwordConfirm" 
									ref="passwordConfirm" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required', 'password']} 
								/>
							</FormGroup>

						</div>
					</div>

					<br/>

					<div className="">

						<Validation.components.Button 
							type="submit"
							className="btn btn-primary">
							<i className="fa fa-save icon left"></i>
							Save
						</Validation.components.Button>

						<button 
							type="reset" 
							className="btn btn-link" 
							onClick={this.handleReset.bind(this)}>
							Reset
						</button>
						
					</div>

				</Validation.components.Form>

				<NotificationContainer />

			</Panel>
		);
	
	}

}

export default Credentials;
