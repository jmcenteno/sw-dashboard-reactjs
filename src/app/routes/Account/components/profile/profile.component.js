import React from 'react';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';
import { NotificationContainer, NotificationManager } from 'react-notifications';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	}
});

class Profile extends React.Component {

	static propTypes = {
		data: React.PropTypes.shape({
			firstName: React.PropTypes.string.isRequired,
			lastName: React.PropTypes.string.isRequired,
			picture: React.PropTypes.string
		}).isRequired,
		updated: React.PropTypes.any,
		onSubmit: React.PropTypes.func.isRequired,
		onReset: React.PropTypes.func.isRequired,
		dismissAlert: React.PropTypes.func.isRequired
	};

	constructor (props) {

		super(props);

		this.state = {
			profile: this.props.data
		};

	}

	componentWillReceiveProps (newProps) {

		this.setState({
			profile: newProps.data
		});

	}

	componentDidUpdate () {

		if (this.props.updated) {
			
			NotificationManager.success('Your profile has been updated.', 'Success!');

			setTimeout(() => {
				this.props.dismissAlert();
			}, 3000);

		}

	}

	handleSubmit (e) {

		e.preventDefault();

		const profile = Object.assign({}, this.state.profile, {
			firstName: this.refs.firstName.state.value,
			lastName: this.refs.lastName.state.value,
			picture: this.state.profile.picture || null
		});

		this.props.onSubmit(profile);

	}

	handleReset () {

		this.props.onReset();

	}

	openFileBrowser () {

		document.getElementById('file-input').click();

	}

	setProfilePicture (e) {

		const reader = new FileReader();

		reader.addEventListener('load', (event) => {
			
			const profile = Object.assign({}, this.state.profile);
			profile.picture = event.target['result'].split(',')[1];
			
			this.setState({ profile: profile });

		}, false);

		reader.readAsDataURL(e.target.files[0]);

	}

	render () {

		const { profile } = this.state;
		const picture = (
			profile && profile.picture ? 
			'data:image/png;base64,' + profile.picture : 
			'../img/no-img.png'
		);
		
		return (
			<Panel>
				
				{
					profile ? 
					<Validation.components.Form 
						ref={(c) => { this.form = c }} 
						onSubmit={(e) => this.handleSubmit(e)}>

						<div className="row">
							<div className="col-md-6 col-lg-4">

								<div className="profile-picture text-center">
									<img 
										src={picture} 
										alt="Profile Picture" 
										className={(!this.state.profile.picture ? 'img-circle' : '')} 
									/>
									<br />
									<button type="button" className="btn btn-default btn-sm" onClick={this.openFileBrowser}>
										<i className="fa fa-camera"></i>
										Upload Photo
									</button>
									<input 
										type="file" 
										name="picture" 
										ref="picture"
										id="file-input" 
										className="hidden" 
										onChange={this.setProfilePicture.bind(this)}
									/>
								</div>

							</div>
							<div className="col-md-6 col-lg-4">

								<FormGroup controlId="firstName">
									<ControlLabel>First Name</ControlLabel>
									<Validation.components.Input 
										value={profile.firstName} 
										name="firstName" 
										ref="firstName"
										className="form-control" 
										errorClassName="invalid"
										validations={['required']} 
									/>
								</FormGroup>

								<FormGroup controlId="lastName">
									<ControlLabel>Last Name</ControlLabel>
									<Validation.components.Input 
										value={profile.lastName} 
										name="lastName" 
										ref="lastName" 
										className="form-control" 
										errorClassName="invalid"
										validations={['required']} 
									/>
								</FormGroup>

							</div>
						</div>

						<br/>

						<div className="">
							
							<Validation.components.Button 
								type="submit"
								className="btn btn-primary">
								<i className="fa fa-save icon left"></i>
								Save
							</Validation.components.Button>
							
							<button 
								type="button" 
								className="btn btn-link" 
								onClick={this.handleReset.bind(this)}>
								Reset
							</button>

						</div>

					</Validation.components.Form> :
					<Spinner />
				}

				<NotificationContainer />

			</Panel>
		);
	}

}

export default Profile;
