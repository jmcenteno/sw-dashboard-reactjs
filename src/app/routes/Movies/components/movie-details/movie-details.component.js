import React from 'react';
import { Panel } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, Spinner } from '../../../../components';
import MovieForm from '../movie-form';

import './movie-details.styles.scss';

export class MovieDetailsView extends React.Component {

	componentDidMount () {
		
		this.props.actions.getMovieDetails(this.props.params.id);

	}

	componentDidUpdate () {

		if (this.props.updated) {
			NotificationManager.success(this.props.movieDetails.title + ' has been updated.', 'Success!');
		}

	}

	render () {

		const movie = this.props.movieDetails;

		const pageHeader = (
			<h1 className="page-title">
				Movies
				<br/>
				{
					movie && movie.title ?
					<small>{movie.title}</small> :
					null
				}
			</h1>
		);

		const panelHeader = (
			movie ?
			<h3 className="panel-title">Movie ID: {movie.id}</h3> :
			null
		);
	
		return (
			<Page header={pageHeader}>
				
				<div className="row">
					<div className="col-sm-12">
						<Panel header={panelHeader}>
							{
								movie && movie.id ?	
								<div className="row">
									<div className="col-md-8">
										<MovieForm 
											movie={movie} 
											onSubmit={this.props.actions.updateMovie.bind(this)}
											onCancel={() => this.props.router.push('/movies')} 
										/>
									</div>
								</div> : 
								<Spinner />
							}
						</Panel>
					</div>
				</div>

				<NotificationContainer />

			</Page>
		);

	}

}

export default MovieDetailsView;
