import React from 'react';
import { Link } from 'react-router';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	}
});


class MovieForm extends React.Component {

	static propTypes = {
		movie: React.PropTypes.object.isRequired,
		onSubmit: React.PropTypes.func.isRequired,
		onCancel: React.PropTypes.func.isRequired
	};

	handleSubmit (e) {

		e.preventDefault();

		// form data
		const data = {};

		this.props.onSubmit(data);

	}

	render () {

		const movie = this.props.movie;
	
		return (						
			<Validation.components.Form 
				ref={(c) => { this.form = c }} 
				onSubmit={(e) => this.handleSubmit(e)}>

				<fieldset>
				
					<FormGroup controlId="title">
						<ControlLabel>Title</ControlLabel>
						<Validation.components.Input 
							value={movie.title} 
							name="title" 
							className="form-control" 
							errorClassName="invalid"
							validations={['required']} 
						/>
					</FormGroup>

					<FormGroup controlId="director">
						<ControlLabel>Director</ControlLabel>
						<Validation.components.Input 
							value={movie.director} 
							name="director" 
							className="form-control" 
							errorClassName="invalid"
							validations={['required']} 
						/>
					</FormGroup>

					<FormGroup controlId="producer">
						<ControlLabel>Producer</ControlLabel>
						<Validation.components.Input 
							value={movie.producer} 
							name="producer" 
							className="form-control"
							errorClassName="invalid" 
							validations={['required']} 
						/>
					</FormGroup>

					<div className="row">
						<div className="col-sm-4">
							
							<FormGroup controlId="release_date">
								<ControlLabel>Release Date</ControlLabel>
								<Validation.components.Input 
									value={movie.release_date} 
									name="release_date" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']} 
								/>
							</FormGroup>

						</div>
					</div>

				</fieldset>

				<br/>

				<div className="">
					
					<Validation.components.Button 
						type="submit"
						className="btn btn-primary">
						<i className="fa fa-save icon left"></i>
						Save
					</Validation.components.Button>
					
					<button
						type="button" 
						className="btn btn-link" 
						onClick={() => this.props.onCancel()}>
						Cancel
					</button>

				</div>				
			
			</Validation.components.Form>
		);

	}

}

export default MovieForm;
