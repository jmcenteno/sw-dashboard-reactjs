import React from 'react';
import { Link } from 'react-router';
import { Panel, Modal } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, SearchBar, Spinner, SortableTable } from '../../../../components';
import { Movie, Utils } from '../../../../services';
import MovieForm from '../movie-form';

import './movie-list.styles.scss'

export class MoviesView extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			filter: '',
			showCreateModal: false
		};

	}

	componentDidMount () {
		
		this.props.actions.getMovies();

	}

	componentDidUpdate () {

		if (this.props.created) {
			NotificationManager.success('A new movie has been created.', 'Success!');
			this.closeCreateModal();
		}

	}

	showCreateModal () {

		this.setState({ 'showCreateModal': true });

	}

	closeCreateModal () {

		this.setState({ 'showCreateModal': false });

	}

	getPageHeader () {

		return (
			<div className="row">
				<div className="col-sm-8">
					<h1 className="page-title pull-left">Movies</h1>
				</div>
				<div className="col-sm-4">
					<button 
						type="button" 
						className="btn btn-default btn-sm pull-right" 
						onClick={() => this.showCreateModal()}>
						<i className="fa fa-plus icon left"></i>
						Add Movie
					</button>
				</div>
			</div>
		);

	}

	getColumns () {

		return [
			{ key: 'episode_id', label: 'Episode', sortable: true },
			{ key: 'title', label: 'Title', sortable: true },
			{ key: 'release_date', label: 'Release Date', sortable: true },
			{ key: 'actions', label: '', sortable: false },
		];

	}

	getRows () {

		const data = this.props.movies;
		const filter = this.state.filter;
		const fields = ['episode_id', 'title', 'release_date'];

		return Utils.filterDataSet(data, filter, fields).map((row) => {
			
			row.actions = (
				<div>
					<Link to={`/movies/${row.id}`} className="btn btn-default btn-xs">
						<i className="fa fa-pencil"></i>
					</Link>
					<button type="button" className="btn btn-default btn-xs">
						<i className="fa fa-trash"></i>
					</button>
				</div>
			);

			return row;

		});

	}

	render () {

		const movies = this.props.movies;
		const pageHeader = this.getPageHeader();
		const columns = this.getColumns();
		const rows = this.getRows();
	
		return (
			<Page header={pageHeader}>

				<SearchBar 
					filter={this.state.filter} 
					setFilter={(value) => this.setState({ filter: value })}
				/>

				<Panel>
					{
						movies.length ?
						<div className="table-responsive">
							<SortableTable columns={columns} rows={rows} />
						</div> :
						<Spinner />
					}
				</Panel>

				<Modal show={this.state.showCreateModal} onHide={() => this.closeCreateModal()}>
					<Modal.Header closeButton>
						<Modal.Title>Create Movie</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<MovieForm 
							movie={new Movie()} 
							onSubmit={this.props.actions.createMovie.bind(this)} 
							onCancel={() => this.closeCreateModal()}
						/>
					</Modal.Body>
				</Modal>

				<NotificationContainer />

			</Page>
		);

	}

}

export default MoviesView;
