import { injectReducer } from '../../store/reducers';
import { Auth } from '../../services';

function MovieList(store) {

	return {
		path: '/movies',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
			and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
				dependencies for bundling   */
				const MovieList = require('./containers/movie-list.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'movies', reducer });

				/*  Return getComponent   */
				cb(null, MovieList);

				/* Webpack named bundle   */
			}, 'movies');

		},
		onEnter: function (nextState, replace, callback) {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

function MovieDetails(store) {

	return {
		path: '/movies/:id',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
			and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
				dependencies for bundling   */
				const MovieDetails = require('./containers/movie-details.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'movieDetails', reducer });

				/*  Return getComponent   */
				cb(null, MovieDetails);

				/* Webpack named bundle   */
			}, 'movieDetails');

		},
		onEnter: function (nextState, replace, callback) {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

export {
	MovieList,
	MovieDetails
}
