import * as Constants from './constants';

const ACTION_HANDLERS = {
	
	[Constants.GET_MOVIES]: function (state, action) {
		return Object.assign({}, state, {
			movies: action.payload || []
		});
	},
	
	[Constants.GET_MOVIE_DETAILS]: function (state, action) {
		return Object.assign({}, state, {
			movieDetails: action.payload || null
		});
	},

	[Constants.MOVIE_CREATED]: function (state, action) {
		console.log(action.payload)
		return Object.assign({}, state, {
			created: action.payload || null
		});
	},

	[Constants.MOVIE_UPDATED]: function (state, action) {
		return Object.assign({}, state, {
			updated: action.payload || null
		});
	},

}

const initialState = {
	movies: [],
	movieDetails: null,
	created: null,
	updated: null
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
