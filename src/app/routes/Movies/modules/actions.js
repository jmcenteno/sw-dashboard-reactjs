import _ from 'lodash';

import { Movie, MoviesService } from '../../../services';
import * as Constants from './constants';

export function getMovies () {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.GET_MOVIES,
            payload: []
        });
        
        MoviesService.getMovies().then((data) => {
                
            dispatch({
                type: Constants.GET_MOVIES,
                payload: _.sortBy(data, ['episode_id'])
            });
           
        });
        
    };
}

export function getMovieDetails (id) {
    return (dispatch, getState) => {

    	dispatch({
            type: Constants.GET_MOVIE_DETAILS,
            payload: new Movie()
        });
        
        MoviesService.getMovieDetails(id).then((data) => {
                
            dispatch({
                type: Constants.GET_MOVIE_DETAILS,
                payload: data
            });
           
        });
        
    };
}

export function createMovie (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.MOVIE_CREATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.MOVIE_CREATED,
                payload: null
            });

        }, 500);

    };
}

export function updateMovie (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.MOVIE_UPDATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.MOVIE_UPDATED,
                payload: null
            });

        }, 500);

    };
}

export const actions = {
    getMovies,
    getMovieDetails,
    createMovie,
    updateMovie
}
