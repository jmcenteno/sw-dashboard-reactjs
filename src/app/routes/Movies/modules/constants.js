export const GET_MOVIES = 'GET_MOVIES';
export const GET_MOVIE_DETAILS = 'GET_MOVIE_DETAILS';
export const CREATE_MOVIE = 'CREATE_MOVIE';
export const MOVIE_CREATED = 'MOVIE_CREATED';
export const UPDATE_MOVIE_DETAILS = 'UPDATE_MOVIE_DETAILS';
export const MOVIE_UPDATED = 'MOVIE_UPDATED';
