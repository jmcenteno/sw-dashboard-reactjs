import _ from 'lodash';

import { PlanetsService, Planet } from '../../../services';
import * as Constants from './constants';

export function getPlanets () {
    return (dispatch, getState) => {

    	dispatch({
    		type: Constants.GET_PLANETS,
    		payload: []
    	});
        
        PlanetsService.getPlanets().then((data) => {
                
            dispatch({
                type: Constants.GET_PLANETS,
                payload: _.sortBy(data, ['name'])
            });
           
        });
        
    };
}

export function getPlanetDetails (id) {
    return (dispatch, getState) => {

    	dispatch({
            type: Constants.GET_PLANET_DETAILS,
            payload: new Planet()
        });
        
        PlanetsService.getPlanetDetails(id)
            .then((data) => {
                
                dispatch({
                    type: Constants.GET_PLANET_DETAILS,
                    payload: data
                });
               
            });
        
    };
}

export function createPlanet (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.PLANET_CREATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.PLANET_CREATED,
                payload: null
            });

        }, 500);

    };
}

export function updatePlanet (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.PLANET_UPDATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.PLANET_UPDATED,
                payload: null
            });

        }, 500);

    };
}

export const actions = {
    getPlanets,
    getPlanetDetails,
    createPlanet,
    updatePlanet
}
