export const GET_PLANETS = 'GET_PLANETS';
export const GET_PLANET_DETAILS = 'GET_PLANET_DETAILS';
export const PLANET_CREATED = 'PLANET_CREATED';
export const PLANET_UPDATED = 'PLANET_UPDATED';
