import * as Constants from './constants';

const ACTION_HANDLERS = {
	
	[Constants.GET_PLANETS]: (state, action) => {
		return Object.assign({}, state, {
			planets: action.payload
		});
	},

	[Constants.GET_PLANET_DETAILS]: (state, action) => {
		return Object.assign({}, state, {
			planetDetails: action.payload
		});
	},

	[Constants.PLANET_CREATED]: (state, action) => {
		console.log(action.payload)
		return Object.assign({}, state, {
			created: action.payload
		});
	},

	[Constants.PLANET_UPDATED]: (state, action) => {
		return Object.assign({}, state, {
			updated: action.payload
		});
	},

}

const initialState = {
	planets: [],
	planetDetails: null,
	created: null,
	updated: null
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
