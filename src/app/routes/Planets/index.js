import { injectReducer } from '../../store/reducers';
import { Auth } from '../../services';

function PlanetList(store) {

	return {
		path: '/planets',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
			and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
				dependencies for bundling   */
				const container = require('./containers/planet-list.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'planets', reducer });

				/*  Return getComponent   */
				cb(null, container);

				/* Webpack named bundle   */
			}, 'planets');

		},
		onEnter: (nextState, replace, callback) => {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

function PlanetDetails(store) {

	return {
		path: '/planets/:id',
		/*  Async getComponent is only invoked when route matches   */
		getComponent(nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
			    and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
					dependencies for bundling   */
				const PlanetDetails = require('./containers/planet-details.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'planetDetails', reducer });

				/*  Return getComponent   */
				cb(null, PlanetDetails);

				/* Webpack named bundle   */
			}, 'planetDetails');

		},
		onEnter: (nextState, replace, callback) => {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

export {
	PlanetList,
	PlanetDetails
}
