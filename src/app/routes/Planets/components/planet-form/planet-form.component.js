import React from 'react';
import { Link } from 'react-router';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';
import Select from 'react-select';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	}
});


class PlanetForm extends React.Component {

	static propTypes = {
		planet: React.PropTypes.object.isRequired,
		onSubmit: React.PropTypes.func.isRequired,
		onCancel: React.PropTypes.func.isRequired
	};

	constructor (props) {

		super(props);

		this.state = {
			terrains: this.props.planet.terrain || []
		};

	}

	handleSubmit (e) {

		e.preventDefault();

		const data = {};

		this.props.onSubmit(data);

	}

	render () {

		const {
			planet,
			options
		} = this.props;
	
		return (						
			<Validation.components.Form 
				ref={(c) => { this.form = c }} 
				onSubmit={(e) => this.handleSubmit(e)}>

				<fieldset>
				
					<FormGroup controlId="name">
						<ControlLabel>Name</ControlLabel>
						<Validation.components.Input 
							value={planet.name} 
							name="name" 
							className="form-control" 
							errorClassName="invalid"
							validations={['required']} 
						/>
					</FormGroup>

				</fieldset>

				<fieldset>

					<div className="row">
						<div className="col-sm-6">

							<FormGroup controlId="population">
								<ControlLabel>Population</ControlLabel>
								<Validation.components.Input 
									value={planet.population} 
									name="population" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
					</div>

				</fieldset>

				<fieldset>

					<div className="row">
						<div className="col-sm-6">

							<FormGroup controlId="climate">
								<ControlLabel>Climate</ControlLabel>
								<Validation.components.Input 
									value={planet.climate} 
									name="climate" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
						<div className="col-sm-6">

							<FormGroup controlId="surface_water">
								<ControlLabel>Surface Water</ControlLabel>
								<Validation.components.Input 
									value={planet.surface_water} 
									name="surface_water" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
					</div>

					<FormGroup controlId="terrain">
						<ControlLabel>Terrain</ControlLabel>
						<Select
							multi={true}
						    name="terrain"
						    placeholder="Select Terrain(s)s"
						    value={this.state.terrains}
						    options={options.terrains.map((item) => ({ value: item, label: item }))}
						    onChange={(value) => this.setState({ terrains: value })}
						/>
					</FormGroup>

				</fieldset>

				<fieldset>

					<div className="row">
						<div className="col-sm-3">

							<FormGroup controlId="rotation_period">
								<ControlLabel>Rotation Period</ControlLabel>
								<Validation.components.Input 
									value={planet.rotation_period} 
									name="rotation_period" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
						<div className="col-sm-3">

							<FormGroup controlId="orbital_period">
								<ControlLabel>Orbital Period</ControlLabel>
								<Validation.components.Input 
									value={planet.orbital_period} 
									name="orbital_period" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
						<div className="col-sm-3">
							
							<FormGroup controlId="diameter">
								<ControlLabel>Diameter</ControlLabel>
								<Validation.components.Input 
									value={planet.diameter} 
									name="diameter" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
						<div className="col-sm-3">

							<FormGroup controlId="gravity">
								<ControlLabel>Gravity</ControlLabel>
								<Validation.components.Input 
									value={planet.gravity} 
									name="gravity" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
								/>
							</FormGroup>

						</div>
					</div>

				</fieldset>

				<br/>

				<div className="">
					
					<Validation.components.Button 
						type="submit" 
						className="btn btn-primary">
						<i className="fa fa-save icon left"></i>
						Save
					</Validation.components.Button>
					
					<button
						type="button" 
						className="btn btn-link" 
						onClick={() => this.props.onCancel()}>
						Cancel
					</button>
					
				</div>				
			
			</Validation.components.Form>
		);

	}

}

export default PlanetForm;
