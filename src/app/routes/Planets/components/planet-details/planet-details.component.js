import React from 'react';
import { Panel } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, Spinner } from '../../../../components';
import { PlanetsService } from '../../../../services';
import PlanetForm from '../planet-form';

import './planet-details.styles.scss';

export class PlanetDetailsView extends React.Component {

	componentDidMount () {
		
		this.props.actions.getPlanets();
		this.props.actions.getPlanetDetails(this.props.params.id);

	}

	componentDidUpdate () {

		if (this.props.updated) {
			NotificationManager.success(this.props.planetDetails.name + ' has been updated.', 'Success!');
		}

	}

	render () {

		const {
			planets,
			planetDetails
		} = this.props;

		const options = PlanetsService.getOptions(planets);

		const pageHeader = (
			<h1 className="page-title">
				Planets
				<br/>
				{
					planetDetails && planetDetails.name ?
					<small>{planetDetails.name}</small> :
					null
				}
			</h1>
		);

		const panelHeader = (
			planetDetails ?
			<h3 className="panel-title">Planet ID: {planetDetails.id}</h3> :
			null
		);
	
		return (
			<Page header={pageHeader}>
				
				<div className="row">
					<div className="col-sm-12">
						<Panel header={panelHeader}>
							{
								planetDetails && planetDetails.id ?	
								<div className="row">
									<div className="col-md-8">
										<PlanetForm 
											planet={planetDetails} 
											options={options}
											onSubmit={this.props.actions.updatePlanet.bind(this)}
											onCancel={() => this.props.router.push('/planets')} 
										/>
									</div>
								</div> : 
								<Spinner />
							}
						</Panel>
					</div>
				</div>

				<NotificationContainer />

			</Page>
		);

	}

}

export default PlanetDetailsView;
