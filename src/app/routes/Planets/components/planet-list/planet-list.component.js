import React from 'react';
import { Link } from 'react-router';
import { Panel, Modal } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, SearchBar, Spinner, SortableTable } from '../../../../components';
import { Planet, PlanetsService, Utils } from '../../../../services';
import PlanetForm from '../planet-form';

import './planet-list.styles.scss'

export class PlanetsView extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			filter: '',
			showCreateModal: false
		};

	}

	componentDidMount () {
		
		this.props.actions.getPlanets();

	}

	componentDidUpdate () {

		if (this.props.created) {
			NotificationManager.success('A new planet has been created.', 'Success!');
			this.closeCreateModal();
		}

	}

	showCreateModal () {

		this.setState({ 'showCreateModal': true });

	}

	closeCreateModal () {

		this.setState({ 'showCreateModal': false });

	}

	getPageHeader () {
		
		return (
			<div className="row">
				<div className="col-sm-8">
					<h1 className="page-title">Planets</h1>
				</div>
				<div className="col-sm-4">
					<button 
						type="button" 
						className="btn btn-default btn-sm pull-right" 
						onClick={() => this.showCreateModal()}>
						<i className="fa fa-plus icon left"></i>
						Add Planets
					</button>
				</div>
			</div>
		);

	}

	getColumns () {

		return [
			{ key: 'name', label: 'Name', sortable: true },
			{ key: 'diameter', label: 'Diameter', sortable: true },
			{ key: 'climate', label: 'Climate', sortable: true },
			{ key: 'population', label: 'Population', sortable: true },
			{ key: 'actions', label: '', sortable: false },
		];

	}

	getRows () {

		const data = this.props.planets;
		const filter = this.state.filter;
		const fields = ['name', 'diameter', 'climate', 'population'];

		return Utils.filterDataSet(data, filter, fields).map((row) => {

			row.diameter = (
				row.diameter && row.diameter != 'unknown' ? 
				Number.parseInt(row.diameter, 10) : 
				null
			);

			row.population = (
				row.population && row.population != 'unknown' ? 
				Number.parseInt(row.population, 10) : 
				null
			);
			
			row.actions = (
				<div>
					<Link to={`/planets/${row.id}`} className="btn btn-default btn-xs">
						<i className="fa fa-pencil"></i>
					</Link>
					<button type="button" className="btn btn-default btn-xs">
						<i className="fa fa-trash"></i>
					</button>
				</div>
			);

			return row;

		});

	}

	render () {

		const planets = this.props.planets;
		const pageHeader = this.getPageHeader();
		const columns = this.getColumns();
		const rows = this.getRows();

		console.log(this.props)
	
		return (
			<Page header={pageHeader}>

				<SearchBar 
					filter={this.state.filter} 
					setFilter={(value) => this.setState({ filter: value })}
				/>

				<Panel>
					{
						planets.length ?
						<div className="table-responsive">
							<SortableTable columns={columns} rows={rows} />
						</div> :
						<Spinner />
					}
				</Panel>

				<Modal show={this.state.showCreateModal} onHide={() => this.closeCreateModal()}>
					<Modal.Header closeButton>
						<Modal.Title>Create Planets</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<PlanetForm 
							planet={new Planet()} 
							options={PlanetsService.getOptions(planets || [])}
							onSubmit={this.props.actions.createPlanet.bind(this)} 
							onCancel={() => this.closeCreateModal()}
						/>
					</Modal.Body>
				</Modal>

				<NotificationContainer />

			</Page>
		);

	}

}

export default PlanetsView;
