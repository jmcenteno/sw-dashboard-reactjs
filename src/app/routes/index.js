// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout';
import Home from './Home';
import Login from './Login';
import Account from './Account';
import * as Movies from './Movies';
import * as People from './People';
import * as Species from './Species';
import * as Planets from './Planets';
import CounterRoute from './Counter';

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
	path: '/',
	component: CoreLayout,
	indexRoute: Home(store),
	childRoutes: [
		Login(store),
		Account(store),
		Movies.MovieList(store),
		Movies.MovieDetails(store),
		People.PeopleList(store),
		People.PersonDetails(store),
		Species.SpeciesList(store),
		Species.SpeciesDetails(store),
		Planets.PlanetList(store),
		Planets.PlanetDetails(store),
		CounterRoute(store)
	]
});

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes;
