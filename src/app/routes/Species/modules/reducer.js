import * as Constants from './constants';

const ACTION_HANDLERS = {

	[Constants.GET_SPECIES]: (state, action) => {
		return Object.assign({}, state, {
			species: action.payload
		});
	},

	[Constants.GET_SPECIES_DETAILS]: (state, action) => {
		return Object.assign({}, state, {
			speciesDetails: action.payload
		});
	},

	[Constants.SPECIES_CREATED]: (state, action) => {
		return Object.assign({}, state, {
			created: action.payload
		});
	},

	[Constants.SPECIES_UPDATED]: (state, action) => {
		return Object.assign({}, state, {
			updated: action.payload
		});
	},

}

const initialState = {
	species: [],
	speciesDetails: null,
	created: null,
	updated: null
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
