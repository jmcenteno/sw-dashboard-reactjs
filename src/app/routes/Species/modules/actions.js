import _ from 'lodash';

import { Species, SpeciesService } from '../../../services';
import * as Constants from './constants';

export function getSpecies () {
    return (dispatch, getState) => {

    	dispatch({
            type: Constants.GET_SPECIES,
            payload: []
        });
        
        SpeciesService.getSpecies()
        	.then((data) => {
                
	            dispatch({
	                type: Constants.GET_SPECIES,
	                payload: _.sortBy(data, ['name'])
	            });
	           
	        });
        
    };
}

export function getSpeciesDetails (id) {
    return (dispatch, getState) => {

    	dispatch({
            type: Constants.GET_SPECIES_DETAILS,
            payload: new Species()
        });
        
        SpeciesService.getSpeciesDetails(id).then((data) => {
                
            dispatch({
                type: Constants.GET_SPECIES_DETAILS,
                payload: data
            });
           
        });
        
    };
}

export function createSpecies (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.SPECIES_CREATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.SPECIES_CREATED,
                payload: null
            });

        }, 500);

    };
}

export function updateSpecies (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.SPECIES_UPDATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.SPECIES_UPDATED,
                payload: null
            });

        }, 500);

    };
}

export const actions = {
    getSpecies,
    getSpeciesDetails,
    createSpecies,
    updateSpecies
}
