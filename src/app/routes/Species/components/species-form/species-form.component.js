import React from 'react';
import { Link } from 'react-router';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';
import Select from 'react-select';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	}
});


class SpeciesForm extends React.Component {

	static propTypes = {
		species: React.PropTypes.object.isRequired,
		onSubmit: React.PropTypes.func.isRequired,
		onCancel: React.PropTypes.func.isRequired
	};

	constructor (props) {

		super(props);

		this.state = {
			hairColors: [],
			eyeColors: [],
			skinColors: []
		};

	}

	handleSubmit (e) {

		e.preventDefault();

		const data = {};

		this.props.onSubmit(data);

	}

	render () {

		const {
			species,
			options
		} = this.props;
	
		return (						
			<Validation.components.Form 
				ref={(c) => { this.form = c }} 
				onSubmit={(e) => this.handleSubmit(e)}>

				<fieldset>
				
					<FormGroup controlId="name">
						<ControlLabel>Name</ControlLabel>
						<Validation.components.Input 
							value={species.name} 
							name="name" 
							className="form-control" 
							errorClassName="invalid"
							validations={['required']} 
						/>
					</FormGroup>

					<div className="row">
						<div className="col-sm-4">

							<FormGroup controlId="classification">
								<ControlLabel>Classification</ControlLabel>
								<Validation.components.Select 
									value={species.classification} 
									name="classification" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}>
									<option value="">Select One</option>
									{
										options.classifications.map((item, i) => {
											return (
												<option 
													key={`classification-opt-${i}`} 
													value={item}>
													{item}
												</option>
											);
										})
									}
								</Validation.components.Select>
							</FormGroup>

						</div>
						<div className="col-sm-4">

							<FormGroup controlId="designation">
								<ControlLabel>Designation</ControlLabel>
								<Validation.components.Select 
									value={species.designation} 
									name="designation" 
									className="form-control"
									errorClassName="invalid" 
									validations={['required']}>
									<option value="">Select One</option>
									{
										options.designations.map((item, i) => {
											return (
												<option 
													key={`designation-opt-${i}`} 
													value={item}>
													{item}
												</option>
											);
										})
									}
								</Validation.components.Select>
							</FormGroup>

						</div>
						<div className="col-sm-4">
							
							<FormGroup controlId="language">
								<ControlLabel>Language</ControlLabel>
								<Validation.components.Select 
									value={species.language} 
									name="language" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}>
									<option value="">Select One</option>
									{
										options.languages.map((item, i) => {
											return (
												<option 
													key={`language-opt-${i}`} 
													value={item}>
													{item}
												</option>
											);
										})
									}
								</Validation.components.Select>
							</FormGroup>

						</div>
					</div>

				</fieldset>

				<fieldset>

					<FormGroup controlId="hair_colors">
						<ControlLabel>Hair Colors</ControlLabel>
						<Select
							multi={true}
						    name="hair_colors"
						    placeholder="Select Hair Color(s)"
						    value={this.state.hairColors}
						    options={options.hairColors.map((item) => ({ value: item, label: item }))}
						    onChange={(value) => this.setState({ hairColors: value })}
						/>
					</FormGroup>

					<FormGroup controlId="skin_colors">
						<ControlLabel>Skin Colors</ControlLabel>
						<Select
							multi={true}
						    name="skin_colors"
						    placeholder="Select Skin Color(s)"
						    value={this.state.skinColors}
						    options={options.skinColors.map((item) => ({ value: item, label: item }))}
						    onChange={(value) => this.setState({ skinColors: value })}
						/>
					</FormGroup>

					<FormGroup controlId="eye_colors">
						<ControlLabel>Eye Colors</ControlLabel>
						<Select
							multi={true}
						    name="eye_colors"
						    placeholder="Select Eye Color(s)"
						    value={this.state.eyeColors}
						    options={options.eyeColors.map((item) => ({ value: item, label: item }))}
						    onChange={(value) => this.setState({ eyeColors: value })}
						/>
					</FormGroup>

				</fieldset>

				<br/>

				<div className="">
					
					<Validation.components.Button 
						type="submit" 
						className="btn btn-primary">
						<i className="fa fa-save icon left"></i>
						Save
					</Validation.components.Button>
					
					<button
						type="button" 
						className="btn btn-link" 
						onClick={() => this.props.onCancel()}>
						Cancel
					</button>

				</div>
			
			</Validation.components.Form>
		);

	}

}

export default SpeciesForm;
