import React from 'react';
import { Link } from 'react-router';
import { Panel, Modal } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, SearchBar, Spinner, SortableTable } from '../../../../components';
import { Species, SpeciesService, Utils } from '../../../../services';
import SpeciesForm from '../species-form';

import './species-list.styles.scss'

export class SpeciesView extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			filter: '',
			showCreateModal: false
		};

	}

	componentDidMount () {
		
		this.props.actions.getSpecies();

	}

	componentDidUpdate () {

		if (this.props.created) {
			NotificationManager.success('A new species has been created.', 'Success!');
			this.closeCreateModal();
		}

	}

	showCreateModal () {

		this.setState({ 'showCreateModal': true });

	}

	closeCreateModal () {

		this.setState({ 'showCreateModal': false });

	}

	getPageHeader () {

		return (
			<div className="row">
				<div className="col-sm-8">
					<h1 className="page-title">Species</h1>
				</div>
				<div className="col-sm-4">
					<button 
						type="button" 
						className="btn btn-default btn-sm pull-right" 
						onClick={() => this.showCreateModal()}>
						<i className="fa fa-plus icon left"></i>
						Add Species
					</button>
				</div>
			</div>
		);

	}

	getColumns () {

		return [
			{ key: 'name', label: 'Name', sortable: true },
			{ key: 'classification', label: 'Classification', sortable: true },
			{ key: 'designation', label: 'Designation', sortable: true },
			{ key: 'language', label: 'Language', sortable: true },
			{ key: 'actions', label: '', sortable: false },
		];

	}

	getRows () {

		const data = this.props.species;
		const filter = this.state.filter;
		const fields = ['name', 'classification', 'designation', 'language'];

		return Utils.filterDataSet(data, filter, fields).map((row) => {
			
			row.actions = (
				<div>
					<Link to={`/species/${row.id}`} className="btn btn-default btn-xs">
						<i className="fa fa-pencil"></i>
					</Link>
					<button type="button" className="btn btn-default btn-xs">
						<i className="fa fa-trash"></i>
					</button>
				</div>
			);

			return row;

		});

	}

	render () {

		const species = this.props.species;
		const pageHeader = this.getPageHeader();
		const columns = this.getColumns();
		const rows = this.getRows();
	
		return (
			<Page header={pageHeader}>

				<SearchBar 
					filter={this.state.filter} 
					setFilter={(value) => this.setState({ filter: value })}
				/>

				<Panel fill>
					{
						species.length ?
						<div className="table-responsive">
							<SortableTable columns={columns} rows={rows} />
						</div> :
						<Spinner />
					}
				</Panel>

				<Modal show={this.state.showCreateModal} onHide={() => this.closeCreateModal()}>
					<Modal.Header closeButton>
						<Modal.Title>Create Species</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<SpeciesForm 
							species={new Species()} 
							options={SpeciesService.getOptions(species || [])}
							onSubmit={this.props.actions.createSpecies.bind(this)} 
							onCancel={() => this.closeCreateModal()}
						/>
					</Modal.Body>
				</Modal>

				<NotificationContainer />

			</Page>
		);

	}

}

export default SpeciesView;
