import React from 'react';
import { Link } from 'react-router';
import { Panel } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, Spinner } from '../../../../components';
import { SpeciesService } from '../../../../services';
import SpeciesForm from '../species-form';

import './species-details.styles.scss';

export class SpeciesDetailsView extends React.Component {

	componentDidMount () {
		
		this.props.actions.getSpecies();
		this.props.actions.getSpeciesDetails(this.props.params.id);

	}

	componentDidUpdate () {

		if (this.props.updated) {
			NotificationManager.success(this.props.speciesDetails.name + ' has been updated.', 'Success!');
		}

	}

	render () {

		const {
			species,
			speciesDetails
		} = this.props;

		const options = SpeciesService.getOptions(species);

		const pageHeader = (
			<h1 className="page-title">
				Species
				<br/>
				{
					speciesDetails && speciesDetails.name ?
					<small>{speciesDetails.name}</small> :
					null
				}
			</h1>
		);

		const panelHeader = (
			speciesDetails ?
			<h3 className="panel-title">Species ID: {speciesDetails.id}</h3> :
			null
		);
	
		return (
			<Page header={pageHeader}>
				
				<div className="row">
					<div className="col-sm-12">
						<Panel header={panelHeader}>
							{
								speciesDetails && speciesDetails.id ?	
								<div className="row">
									<div className="col-md-8">
										<SpeciesForm 
											species={speciesDetails} 
											options={options}
											onSubmit={this.props.actions.updateSpecies.bind(this)}
											onCancel={() => this.props.router.push('/species')} 
										/>
									</div>
								</div> : 
								<Spinner />
							}
						</Panel>
					</div>
				</div>

				<NotificationContainer />

			</Page>
		);

	}

}

export default SpeciesDetailsView;
