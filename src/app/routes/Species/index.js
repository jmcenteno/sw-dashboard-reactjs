import { injectReducer } from '../../store/reducers';
import { Auth } from '../../services';

function SpeciesList(store) {

	return {
		path: '/species',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
				and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
					dependencies for bundling   */
				const container = require('./containers/species-list.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'species', reducer });

				/*  Return getComponent   */
				cb(null, container);

				/* Webpack named bundle   */
			}, 'species');

		},
		onEnter: (nextState, replace, callback) => {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

function SpeciesDetails(store) {

	return {
		path: '/species/:id',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
				and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
					dependencies for bundling   */
				const container = require('./containers/species-details.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'speciesDetails', reducer });

				/*  Return getComponent   */
				cb(null, container);

				/* Webpack named bundle   */
			}, 'speciesDetails');

		},
		onEnter: (nextState, replace, callback) => {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

export {
	SpeciesList,
	SpeciesDetails
}
