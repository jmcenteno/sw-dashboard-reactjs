import React from 'react';
import { Link } from 'react-router';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';
import Validator from 'validator';

import { APP_NAME } from '../../../config/constants';
import { Page, Spinner } from '../../../components';

import './login.styles.scss';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	},
	email: {
		// Example usage with external 'validator' 
		rule: value => {
			return Validator.isEmail(value);
		},
		hint: value => {
			return (
				<span className="help-block text-danger">Invalid email.</span>
			);
		}
	},
});

export class LoginView extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			loading: false
		}

	}

	componentWillReceiveProps (newProps) {

		if (newProps.isAuthenticated) {
			this.props.router.push('/');
		}

	}

	handleSubmit (e) {

		e.preventDefault();

		const username = this.refs.username.state.value;
		const password = this.refs.password.state.value;

		this.props.actions.authenticateUser(username, password);

	}

	showModal () {}

	render () {
	
		return (
			<Page className="login">

				<div className="login-form-container center-block">
					<Panel>
						
						<div className="login-header text-center">
							<i className="fa fa-rebel logo text-primary"></i>
							<h1 className="login-title">{APP_NAME}</h1>
						</div>

						{
							this.props.error ?
							<div className="alert alert-danger">
								<p>{this.props.error.message}</p>
							</div> :
							null
						}
						
						<Validation.components.Form 
							ref={(c) => { this.form = c }} 
							onSubmit={(e) => this.handleSubmit(e)}>

							<fieldset>
							
								<FormGroup controlId="username">
									<Validation.components.Input 
										name="username" 
										value="demo@josecenteno.com"
										ref="username"
										placeholder="Username"
										className="form-control" 
										errorClassName="invalid"
										validations={['required', 'email']} 
									/>
								</FormGroup>

								<FormGroup controlId="password">
									<Validation.components.Input 
										name="password" 
										type="password"
										value="#demo123!"
										ref="password"
										placeholder="Password"
										className="form-control" 
										errorClassName="invalid"
										validations={['required']} 
									/>
								</FormGroup>

							</fieldset>

							<br/>

							<div className="">
								<Validation.components.Button className="btn btn-primary">Log In</Validation.components.Button>
							</div>				
						
						</Validation.components.Form>

					</Panel>
				</div>

			</Page>
		);

	}

}

export default LoginView;
