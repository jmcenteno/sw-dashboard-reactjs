import _ from 'lodash';

import { Auth } from '../../../services';
import * as Constants from './constants';

export function authenticateUser (username, password) {
	return (dispatch, getState) => {

		dispatch({
			type: Constants.AUTHENTICATE_USER,
			payload: false
		});

		Auth.authenticate(username, password)
			.then((user) => {

				console.log(user)

				dispatch({
					type: Constants.AUTH_ERROR,
					payload: false
				});

				dispatch({
					type: Constants.AUTHENTICATE_USER,
					payload: (user.uid)
				});

			})
			.catch((error) => {

				console.log(error);

				dispatch({
					type: Constants.AUTH_ERROR,
					payload: error
				});

			});

	};
}

export const actions = {
	authenticateUser
}
