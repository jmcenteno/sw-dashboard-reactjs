import * as Constants from './constants';

const ACTION_HANDLERS = {
	
	[Constants.AUTHENTICATE_USER]: (state, action) => {
		return Object.assign({}, state, {
			isAuthenticated: action.payload
		});
	},

	[Constants.AUTH_ERROR]: (state, action) => {
		return Object.assign({}, state, {
			error: action.payload
		});
	},

}

const initialState = {
	isAuthenticated: false,
	error: false
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
