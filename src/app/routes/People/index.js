import { injectReducer } from '../../store/reducers';
import { Auth } from '../../services';

function PeopleList(store) {

	return {
		path: '/people',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
			and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
				dependencies for bundling   */
				const container = require('./containers/people-list.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'people', reducer });

				/*  Return getComponent   */
				cb(null, container);

				/* Webpack named bundle   */
			}, 'people');

		},
		onEnter: (nextState, replace, callback) => {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

function PersonDetails(store) {

	return {
		path: '/people/:id',
		/*  Async getComponent is only invoked when route matches   */
		getComponent: function (nextState, cb) {

			/*  Webpack - use 'require.ensure' to create a split point
			and embed an async module loader (jsonp) when bundling   */
			require.ensure([], (require) => {

				/*  Webpack - use require callback to define
				dependencies for bundling   */
				const PersonDetails = require('./containers/person-details.container.js').default;
				const reducer = require('./modules/reducer').default;

				/*  Add the reducer to the store on key 'counter'  */
				injectReducer(store, { key: 'personDetails', reducer });

				/*  Return getComponent   */
				cb(null, PersonDetails);

				/* Webpack named bundle   */
			}, 'personDetails');

		},
		onEnter: (nextState, replace, callback) => {

			if (!Auth.isAuthenticated()) {
				replace('/login');
			}

			callback();

		}
	};

}

export {
	PeopleList,
	PersonDetails
}
