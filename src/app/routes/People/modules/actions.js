import _ from 'lodash';

import {
    Person,
    PeopleService,
    SpeciesService,
    PlanetsService
} from '../../../services';

import * as Constants from './constants';

export function getPeople () {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.GET_PEOPLE,
            payload: []
        });
        
        PeopleService.getPeople()
            .then((data) => {
                
                dispatch({
                    type: Constants.GET_PEOPLE,
                    payload: _.sortBy(data, ['name'])
                });
               
            });
        
    };
}

export function getPersonDetails (id) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.GET_PERSON_DETAILS,
            payload: new Person()
        });
        
        PeopleService.getPerson(id)
            .then((data) => {
                
                dispatch({
                    type: Constants.GET_PERSON_DETAILS,
                    payload: data
                });
               
            });
        
    };
}

export function createPerson (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.PERSON_CREATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.PERSON_CREATED,
                payload: null
            });

        }, 500);

    };
}

export function updatePerson (data) {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.PERSON_UPDATED,
            payload: true
        });

        setTimeout(() => {

            dispatch({
                type: Constants.PERSON_UPDATED,
                payload: null
            });

        }, 500);

    };
}

export function getSpecies () {
    return (dispatch, getState) => {

        dispatch({
            type: Constants.GET_SPECIES,
            payload: []
        });
        
        SpeciesService.getSpecies()
            .then((data) => {
                
                dispatch({
                    type: Constants.GET_SPECIES,
                    payload: _.sortBy(data, ['name'])
                });
               
            });
        
    };
}

export function getPlanets () {
    return (dispatch, getState) => {

    	dispatch({
			type: Constants.GET_PLANETS,
			payload: []
		});
        
        PlanetsService.getPlanets()
            .then((data) => {
                
				dispatch({
					type: Constants.GET_PLANETS,
					payload: _.sortBy(data, ['name'])
				});
               
            });
        
    };
}

export const actions = {
    getPeople,
    getPersonDetails,
    createPerson,
    updatePerson,
    getSpecies,
    getPlanets
}
