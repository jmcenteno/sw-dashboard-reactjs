import * as Constants from './constants';

const ACTION_HANDLERS = {
	
	[Constants.GET_PEOPLE]: (state, action) => {
		return Object.assign({}, state, {
			people: action.payload
		});
	},
	
	[Constants.GET_PERSON_DETAILS]: (state, action) => {
		return Object.assign({}, state, {
			personDetails: action.payload
		});
	},

	[Constants.PERSON_CREATED]: (state, action) => {
		return Object.assign({}, state, {
			created: action.payload
		});
	},

	[Constants.PERSON_UPDATED]: (state, action) => {
		return Object.assign({}, state, {
			updated: action.payload
		});
	},
	
	[Constants.GET_SPECIES]: (state, action) => {
		return Object.assign({}, state, {
			species: action.payload
		});
	},
	
	[Constants.GET_PLANETS]: (state, action) => {
		return Object.assign({}, state, {
			planets: action.payload
		});
	},

}

const initialState = {
	people: [],
	personDetails: null,
	created: null,
	updated: null,
	species: [],
	planets: []
};

export default function reducer (state = initialState, action) {
  
	const handler = ACTION_HANDLERS[action.type];

	return (handler ? handler(state, action) : state);

}
