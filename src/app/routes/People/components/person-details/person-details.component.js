import React from 'react';
import { Panel } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, Spinner } from '../../../../components';
import PersonForm from '../person-form';

import './person-details.styles.scss';

export class PersonDetailsView extends React.Component {

	componentDidMount () {
		
		this.props.actions.getPersonDetails(this.props.params.id);
		this.props.actions.getSpecies();
		this.props.actions.getPlanets();

		let timer = setInterval(() => {
			if (this.props.personDetails.name && this.props.species.length) {
				clearInterval(timer);
				this.forceUpdate();
			}
		}, 1000);

	}

	componentDidUpdate () {

		if (this.props.updated) {
			NotificationManager.success(this.props.personDetails.name + ' has been updated.', 'Success!');
		}

	}

	render () {

		const {
			personDetails,
			species,
			planets
		} = this.props;

		const pageHeader = (			
			<h1 className="page-title">
				People
				<br/>
				{
					personDetails && personDetails.name ?
					<small>{personDetails.name}</small> :
					null
				}
			</h1>
		);

		const panelHeader = (
			personDetails ?
			<h3 className="panel-title">Person ID: {personDetails.id}</h3> :
			null
		);
	
		return (
			<Page header={pageHeader}>
				
				<Panel header={panelHeader}>
					{
						personDetails && personDetails.name && species.length && planets.length ?	
						<div className="row">
							<div className="col-md-8">
								<PersonForm 
									model={personDetails} 
									species={species}
									planets={planets}
									onSubmit={this.props.actions.updatePerson.bind(this)}
									onCancel={() => this.props.router.push('/people')} 
								/>
							</div>
						</div> : 
						<Spinner />
					}
				</Panel>

				<NotificationContainer />

			</Page>
		);

	}

}

export default PersonDetailsView;
