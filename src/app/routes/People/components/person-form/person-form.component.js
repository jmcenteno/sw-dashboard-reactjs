import React from 'react';
import { Link } from 'react-router';
import { Panel, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import Validation from 'react-validation';
import _ from 'lodash';

Object.assign(Validation.rules, {
	// Key name maps the rule 
	required: {
		// Function to validate value 
		// NOTE: value might be a number -> force to string 
		rule: (value) => {
			return value.toString().trim();
		},
		// Function to return hint 
		// You may use current value to inject it in some way to the hint 
		hint: (value) => {
			return (
				<span className="help-block text-danger">This field is required.</span>
			);
		}
	}
});


class PersonForm extends React.Component {

	static propTypes = {
		model: React.PropTypes.object.isRequired,
		onSubmit: React.PropTypes.func.isRequired,
		onCancel: React.PropTypes.func.isRequired,
		species: React.PropTypes.arrayOf(React.PropTypes.any).isRequired,
		planets: React.PropTypes.arrayOf(React.PropTypes.any).isRequired
	};

	constructor (props) {

		super(props);

		this.state = {
			hairColors: [],
			eyeColors: [],
			skinColors: []
		};

	}

	componentDidMount () {

		this.updateSpeciesOptions(this.props.model.species.url);

	}

	componentWillReceiveProps (nextProps) {

		this.updateSpeciesOptions(nextProps.model.species.url);

	}

	handleSubmit (e) {

		e.preventDefault();

		this.props.onSubmit(e);

	}

	updateSpeciesOptions (id) {

		this.props.species.forEach((item) => {
			
			if (item.url == id) {

				['hair', 'skin', 'eye'].forEach((key) => {
					
					const arr = item[key + '_colors'].split(', ');
					
					if (this.props.model[key + '_color'] != '' && 
						arr.indexOf(this.props.model[key + '_color']) == -1) 
					{
						item[key + '_colors'] += ', ' + this.props.model[key + '_color'];
					}

				});
			
				this.setState({
					hairColors: item.hair_colors.split(', '),
					eyeColors: item.eye_colors.split(', '),
					skinColors: item.skin_colors.split(', ')
				});

				console.log({
					hairColors: item.hair_colors.split(', '),
					eyeColors: item.eye_colors.split(', '),
					skinColors: item.skin_colors.split(', ')
				})

				return;

			}

		});

	}

	render () {

		const person = this.props.model;
		const species = this.props.species;

		const {
			hairColors,
			skinColors,
			eyeColors
		} = this.state;
	
		return (						
			<Validation.components.Form 
				ref={(c) => { this.form = c }} 
				onSubmit={this.handleSubmit.bind(this)}>
				
				<fieldset>
					<div className="row">
						<div className="col-sm-12">

							<FormGroup controlId="name">
								<ControlLabel className="required">Name</ControlLabel>
								<Validation.components.Input 
									value={person.name || ''} 
									name="name" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']} 
								/>
							</FormGroup>

						</div>
					</div>
				</fieldset>

				<fieldset>
					<div className="row">
						<div className="col-sm-6">

							<FormGroup controlId="gender">
								<ControlLabel className="required">Gender</ControlLabel>
								<Validation.components.Select 
									value={person.gender || ''} 
									name="gender" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}>
									<option value="">Select One</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
									<option value="n/a">N/A</option>
								</Validation.components.Select>
							</FormGroup>

						</div>
						<div className="col-sm-6">

							<FormGroup controlId="birth_year">
								<ControlLabel>Birth Year</ControlLabel>
								<Validation.components.Input 
									value={person.birth_year || ''} 
									name="birth_year" 
									className="form-control" 
									errorClassName="invalid"
									validations={[]} 
								/>
							</FormGroup>

						</div>
					</div>
				</fieldset>

				<fieldset>
					<div className="row">
						<div className="col-sm-6">
							
							<FormGroup controlId="height">
								<ControlLabel>Height</ControlLabel>
								<Validation.components.Input 
									value={person.height || ''} 
									name="height" 
									className="form-control" 
									errorClassName="invalid"
									validations={[]} 
								/>
							</FormGroup>

						</div>
						<div className="col-sm-6">
							
							<FormGroup controlId="mass">
								<ControlLabel>Mass</ControlLabel>
								<Validation.components.Input 
									value={person.mass || ''} 
									name="mass" 
									className="form-control"
									errorClassName="invalid" 
									validations={[]} 
								/>
							</FormGroup>

						</div>
					</div>
				</fieldset>

				<fieldset>
					<div className="row">
						<div className="col-sm-6">
							
							<FormGroup controlId="homeworld">
								<ControlLabel className="required">Homeworld</ControlLabel>
								<Validation.components.Select 
									value={person.homeworld.url || ''} 
									name="homeworld" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}>
									<option value="">Select One</option>
									{
										this.props.planets.map((item, i) => {
											return (
												<option key={'homeworld-opt-' + i} value={item.url}>
													{item.name}
												</option>
											);
										})
									}
								</Validation.components.Select>
							</FormGroup>

						</div>
					</div>
				</fieldset>

				<fieldset>
					<div className="row">
						<div className="col-sm-6">
							
							<FormGroup controlId="species">
								<ControlLabel className="required">Species</ControlLabel>
								<Validation.components.Select 
									value={person.species.url || ''} 
									name="species" 
									className="form-control" 
									errorClassName="invalid"
									validations={['required']}
									onChange={(e) => this.updateSpeciesOptions(e.target.value)}>
									<option value="">Select One</option>
									{
										species.map((item, i) => {
											return (
												<option key={'species-opt-' + i} value={item.url}>
													{item.name}
												</option>
											);
										})
									}
								</Validation.components.Select>
							</FormGroup>

						</div>
					</div>
					<div className="row">
						
						{
							hairColors.length ?
							<div className="col-sm-4">
								<FormGroup controlId="hair_color">
									<ControlLabel>Hair Color</ControlLabel>
									<Validation.components.Select 
										value={person.hair_color || ''} 
										name="hair_color" 
										className="form-control" 
										errorClassName="invalid"
										validations={[]}>
										<option value="">Select One</option>
										{
											hairColors.map((item, i) => {
												return (
													<option key={'hair-color-opt-' + i} value={item}>
														{_.capitalize(item)}
													</option>
												);
											})
										}
									</Validation.components.Select>
								</FormGroup>
							</div> : 
							null
						}
						{
							skinColors.length ?
							<div className="col-sm-4">
								<FormGroup controlId="skin_color">
									<ControlLabel>Skin Color</ControlLabel>
									<Validation.components.Select 
										value={person.skin_color || ''} 
										name="skin_color" 
										className="form-control" 
										errorClassName="invalid"
										validations={[]}>
										<option value="">Select One</option>
										{
											skinColors.map((item, i) => {
												return (
													<option key={'skin-color-opt-' + i} value={item}>
														{_.capitalize(item)}
													</option>
												);
											})
										}
									</Validation.components.Select>
								</FormGroup>
							</div> :
							null
						}

						{
							eyeColors.length ? 
							<div className="col-sm-4">
								<FormGroup controlId="eye_color">
									<ControlLabel>Eye Color</ControlLabel>
									<Validation.components.Select 
										value={person.eye_color || ''} 
										name="eye_color" 
										className="form-control" 
										errorClassName="invalid"
										validations={[]}>
										<option value="">Select One</option>
										{
											eyeColors.map((item, i) => {
												return (
													<option key={'eye-color-opt-' + i} value={item}>
														{_.capitalize(item)}
													</option>
												);
											})
										}
									</Validation.components.Select>
								</FormGroup>
							</div> :
							null
						}

					</div>
				</fieldset>

				<br/>

				<div className="">
					
					<Validation.components.Button 
						type="submit" 
						className="btn btn-primary">
						<i className="fa fa-save icon left"></i>
						Save
					</Validation.components.Button>
					
					<button
						type="button" 
						className="btn btn-link" 
						onClick={() => this.props.onCancel()}>
						Cancel
					</button>

				</div>				
			
			</Validation.components.Form>
		);

	}

}

export default PersonForm;
