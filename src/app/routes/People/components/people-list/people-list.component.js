import React from 'react';
import { Link } from 'react-router';
import { Panel, Modal } from 'react-bootstrap';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { Page, SearchBar, Spinner, SortableTable } from '../../../../components';
import { Person, Utils } from '../../../../services';
import PersonForm from '../person-form';

import './people-list.styles.scss'

export class PeopleListView extends React.Component {

	constructor (props) {

		super(props);

		this.state = {
			filter: '',
			showCreateModal: false
		};

	}

	componentDidMount () {

		this.props.actions.getPeople();
		this.props.actions.getSpecies();
		this.props.actions.getPlanets();

		console.log(this.props)

		const {
			people,
			species,
			planets
		} = this.props;

		let timer = setInterval(() => {
			if (people.length && species.length && planets.length) {
				clearInterval(timer);
				this.forceUpdate();
			}
		}, 1000);

	}

	componentDidUpdate () {

		if (this.props.created) {
			NotificationManager.success('A new person has been created.', 'Success!');
			this.closeCreateModal();
		}

	}

	showCreateModal () {

		this.setState({ 'showCreateModal': true });

	}

	closeCreateModal () {

		this.setState({ 'showCreateModal': false });

	}

	getPageHeader () {

		return (
			<div className="row">
				<div className="col-sm-8">
					<h1 className="page-title pull-left">People</h1>
				</div>
				<div className="col-sm-4">
					<button 
						type="button" 
						className="btn btn-default btn-sm pull-right" 
						onClick={() => this.showCreateModal()}>
						<i className="fa fa-plus icon left"></i>
						Add Person
					</button>
				</div>
			</div>
		);

	}

	getColumns () {

		return [
			{ key: 'name', label: 'Name', sortable: true },
			{ key: 'species', label: 'Species', sortable: true },
			{ key: 'gender', label: 'Gender', sortable: true },
			{ key: 'homeworld', label: 'Homeworld', sortable: true },
			{ key: 'actions', label: '', sortable: false },
		];

	}

	getRows () {

		const data = this.props.people.map((person) => {
			
			this.props.species.forEach((item) => {
				if (item.url == person.species[0]) {
					person.species = item.name;
					return;
				}
			});

			this.props.planets.forEach((item) => {
				if (item.url == person.homeworld) {
					person.homeworld = item.name;
					return;
				}
			});

			return person;

		});

		const filter = this.state.filter;
		const fields = ['name', 'species', 'gender', 'homeworld'];

		return Utils.filterDataSet(data, filter, fields).map((row) => {
			
			row.actions = (
				<div>
					<Link to={`/people/${row.id}`} className="btn btn-default btn-xs">
						<i className="fa fa-pencil"></i>
					</Link>
					<button type="button" className="btn btn-default btn-xs">
						<i className="fa fa-trash"></i>
					</button>
				</div>
			);

			return row;

		});

	}

	render () {

		const people = this.props.people;
		const pageHeader = this.getPageHeader();
		const columns = this.getColumns();
		const rows = this.getRows();
	
		return (
			<Page header={pageHeader}>

				<SearchBar 
					filter={this.state.filter} 
					setFilter={(value) => this.setState({ filter: value })}
				/>

				<Panel>
					{
						people.length ?
						<div className="table-responsive">
							<SortableTable columns={columns} rows={rows} />
						</div> :
						<Spinner />
					}
				</Panel>

				<Modal show={this.state.showCreateModal} onHide={() => this.closeCreateModal()}>
					<Modal.Header closeButton>
						<Modal.Title>Add Person</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<PersonForm 
							model={new Person()} 
							onSubmit={this.props.actions.createPerson.bind(this)} 
							onCancel={() => this.closeCreateModal()} 
							species={this.props.species || []}
							planets={this.props.planets || []}
						/>
					</Modal.Body>
				</Modal>

				<NotificationContainer />

			</Page>
		);

	}

}

export default PeopleListView;
